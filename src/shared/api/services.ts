import { TService } from '../../types/service.type';
import { instance, PARTHER_TOKEN } from './base';
import { TResponse } from './types';

const serviceId = 5095320;

const mockToken = "81736d54d5cfa79f4b08d2d542245a95";

type TGetServiceById = {
  companyId: string | number;
  userToken?: string;
};

export const getServiceById = async ({ companyId, userToken = mockToken }: TGetServiceById) => {
  return await instance.get<TResponse<TService>>(`/company/${companyId}/services/${serviceId}`, {
    headers: {
      Authorization: `Bearer ${PARTHER_TOKEN}, User ${userToken}`,
    },
  });
};
