import React from 'react';

import { Box, Spinner as ChakraSpinner } from '@chakra-ui/react';

export const Spinner = () => (
  <Box height="100%" width="100%" display="flex" alignItems="center" justifyContent="center">
    <ChakraSpinner thickness="4px" speed="0.65s" emptyColor="gray.200" color="blue.500" size="xl" />
  </Box>
);
