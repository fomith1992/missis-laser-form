import { TKeysStores } from '../../../stores';
import { Routes } from '../../enums/routes';

export type TStep = {
  route: Routes;
  index: number;
  stores?: TKeysStores[];
};
