import React from 'react';
import { FaInstagram, FaTelegram, FaVk } from 'react-icons/fa';

import { Box } from '@chakra-ui/layout';
import { Button as ChakraButton } from '@chakra-ui/react';
import { Result } from 'antd';

export const ErrorPage = () => {
  return (
    <Box height='100vh' display='flex' alignItems='center' justifyContent='center'>
      <Result
        status='error'
        title='Произошла ошибка'
        subTitle='Вернитесь и попробуйте еще раз'
        extra={
          <div
            style={{
              display: 'flex',
              alignItems: 'center',
              flexDirection: 'column',
            }}
          >
            <div
              style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'space-between',
                width: '180px',
                alignSelf: 'center',
                marginBottom: '24px',
              }}
            >
              <a href='https://t.me/missislaser'>
                <FaTelegram size={28} color='#0088cc' />
              </a>
              <a href='https://vk.com/missis_laser'>
                <FaVk size={28} color='#0077ff' />
              </a>
              <a href='https://www.instagram.com/missis_laser/'>
                <img alt='inst' src='inst.png' style={{ width: '24px' }} />
              </a>
              <a href='https://dzen.ru/missis-laser.ru'>
                <img alt='dzen' src='dzen.png' style={{ width: '24px' }} />
              </a>
            </div>
          </div>
        }
      />
    </Box>
  );
};
