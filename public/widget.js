/*! For license information please see widget.js.LICENSE.txt */
!(function () {
  "use strict";
  var t = {
      86: function (t, e, n) {
        t.exports = n.p + "images/mixed-logo.gif";
      },
    },
    e = {};
  function n(r) {
    var i = e[r];
    if (void 0 !== i) return i.exports;
    var o = (e[r] = { exports: {} });
    return t[r](o, o.exports, n), o.exports;
  }
  (n.g = (function () {
    if ("object" == typeof globalThis) return globalThis;
    try {
      return this || new Function("return this")();
    } catch (t) {
      if ("object" == typeof window) return window;
    }
  })()),
    (function () {
      var t;
      n.g.importScripts && (t = n.g.location + "");
      var e = n.g.document;
      if (!t && e && (e.currentScript && (t = e.currentScript.src), !t)) {
        var r = e.getElementsByTagName("script");
        r.length && (t = r[r.length - 1].src);
      }
      if (!t) throw new Error("Automatic publicPath is not supported in this browser");
      (t = t
        .replace(/#.*$/, "")
        .replace(/\?.*$/, "")
        .replace(/\/[^\/]+$/, "/")),
        (n.p = t);
    })(),
    (function () {
      var t = function (t, e, n) {
        return t && (e = e.replaceAll("XXXX-XXXX", "".concat(n))), e;
      };
      function e() {
        try {
          if (window.ym) {
            var t = document.cookie.match("(?:^|;)\\s*_ym_uid=([^;]*)");
            return t ? decodeURIComponent(t[1]) : void 0;
          }
        } catch (t) {
          return;
        }
      }
      function r() {
        try {
          if (window.ga) return window.ga.getAll()[0].get("clientId");
        } catch (t) {
          return;
        }
      }
      function i() {
        var t = !1;
        return (
          (function (e) {
            (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(
              e
            ) ||
              /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(
                e.substr(0, 4)
              )) &&
              (t = !0);
          })(navigator.userAgent || navigator.vendor || window.opera),
          t
        );
      }
      function o(e, n) {
        var r = e.appendWazzupId,
          i = e.whatsappGreetingMessage,
          o = e.whatsappNumber,
          a = t(r, i, n);
        return "https://wa.me/".concat(encodeURIComponent(o.replace("+", "")), "?text=").concat(encodeURIComponent(a));
      }
      function a(t, e) {
        return t.telegramUsername.toLowerCase().endsWith("bot")
          ? "https://t.me/".concat(encodeURIComponent(t.telegramUsername), "?start=").concat(e)
          : "https://t.me/".concat(encodeURIComponent(t.telegramUsername));
      }
      var s = Symbol("isLocationPatched");
      var c = "[widget.profeat]";
      function u(t) {
        return fetch("".concat("https://widget.profeat.team", "/api/settings/get-for-external?id=").concat(t))
          .then(function (t) {
            return t.json();
          })
          .then(function (t) {
            return t.data;
          });
      }
      function l(t, n) {
        var o = n.name,
          a = n.wazzupId;
        return fetch("".concat("https://widget.profeat.team", "/api/visitors/create-event"), {
          method: "POST",
          headers: { "Content-Type": "application/json;charset=UTF-8" },
          body: JSON.stringify({
            settings: t,
            wazzupId: a,
            name: o,
            cookies: document.cookie,
            referrer: document.referrer,
            url: window.location.toString(),
            ymId: e(),
            gaId: r(),
            deviceType: i() ? "mobile" : "desktop",
          }),
        })
          .then(function (t) {
            return t.json();
          })
          .then(function (t) {
            return t.data;
          });
      }
      function p(t) {
        return fetch("".concat("https://widget.profeat.team", "/api/integrations/get-by-settings?id=").concat(t))
          .then(function (t) {
            return (
              t.ok ||
                (function () {
                  for (var t, e = arguments.length, n = new Array(e), r = 0; r < e; r++) n[r] = arguments[r];
                  (t = console).warn.apply(t, [c].concat(n));
                })("Can not get integrations. Http", t.status),
              t.json()
            );
          })
          .then(function (t) {
            var e = t.data;
            return null != e ? e : [];
          })
          .catch(function () {
            return [];
          });
      }
      function d(t) {
        return fetch("".concat("https://widget.profeat.team", "/api/visitors/retrieve-dialog-events?id=").concat(t))
          .then(function (t) {
            return t.json();
          })
          .then(function (t) {
            var e = t.data;
            return null != e ? e : [];
          })
          .catch(function () {
            return [];
          });
      }
      function f(t) {
        return fetch("".concat("https://widget.profeat.team", "/api/actions/for-external?id=").concat(t))
          .then(function (t) {
            return t.json();
          })
          .then(function (t) {
            var e = t.data;
            return null != e ? e : [];
          })
          .catch(function () {
            return [];
          });
      }
      let h = t => crypto.getRandomValues(new Uint8Array(t)),
        v = (t, e) =>
          ((t, e, n) => {
            let r = (2 << (Math.log(t.length - 1) / Math.LN2)) - 1,
              i = -~((1.6 * r * e) / t.length);
            return () => {
              let o = "";
              for (;;) {
                let a = n(i),
                  s = i;
                for (; s--; ) if (((o += t[a[s] & r] || ""), o.length === e)) return o;
              }
            };
          })(t, e, h);
      function g(t) {
        for (var e = 1; e < arguments.length; e++) {
          var n = arguments[e];
          for (var r in n) t[r] = n[r];
        }
        return t;
      }
      var y = (function t(e, n) {
          function r(t, r, i) {
            if ("undefined" != typeof document) {
              "number" == typeof (i = g({}, n, i)).expires && (i.expires = new Date(Date.now() + 864e5 * i.expires)),
                i.expires && (i.expires = i.expires.toUTCString()),
                (t = encodeURIComponent(t)
                  .replace(/%(2[346B]|5E|60|7C)/g, decodeURIComponent)
                  .replace(/[()]/g, escape));
              var o = "";
              for (var a in i) i[a] && ((o += "; " + a), !0 !== i[a] && (o += "=" + i[a].split(";")[0]));
              return (document.cookie = t + "=" + e.write(r, t) + o);
            }
          }
          return Object.create(
            {
              set: r,
              get: function (t) {
                if ("undefined" != typeof document && (!arguments.length || t)) {
                  for (var n = document.cookie ? document.cookie.split("; ") : [], r = {}, i = 0; i < n.length; i++) {
                    var o = n[i].split("="),
                      a = o.slice(1).join("=");
                    try {
                      var s = decodeURIComponent(o[0]);
                      if (((r[s] = e.read(a, s)), t === s)) break;
                    } catch (t) {}
                  }
                  return t ? r[t] : r;
                }
              },
              remove: function (t, e) {
                r(t, "", g({}, e, { expires: -1 }));
              },
              withAttributes: function (e) {
                return t(this.converter, g({}, this.attributes, e));
              },
              withConverter: function (e) {
                return t(g({}, this.converter, e), this.attributes);
              },
            },
            { attributes: { value: Object.freeze(n) }, converter: { value: Object.freeze(e) } }
          );
        })(
          {
            read: function (t) {
              return '"' === t[0] && (t = t.slice(1, -1)), t.replace(/(%[\dA-F]{2})+/gi, decodeURIComponent);
            },
            write: function (t) {
              return encodeURIComponent(t).replace(/%(2[346BF]|3[AC-F]|40|5[BDE]|60|7[BCD])/g, decodeURIComponent);
            },
          },
          { path: "/" }
        ),
        m = y;
      const b =
          window.ShadowRoot &&
          (void 0 === window.ShadyCSS || window.ShadyCSS.nativeShadow) &&
          "adoptedStyleSheets" in Document.prototype &&
          "replace" in CSSStyleSheet.prototype,
        w = Symbol(),
        _ = new Map();
      class x {
        constructor(t, e) {
          if (((this._$cssResult$ = !0), e !== w))
            throw Error("CSSResult is not constructable. Use `unsafeCSS` or `css` instead.");
          this.cssText = t;
        }
        get styleSheet() {
          let t = _.get(this.cssText);
          return b && void 0 === t && (_.set(this.cssText, (t = new CSSStyleSheet())), t.replaceSync(this.cssText)), t;
        }
        toString() {
          return this.cssText;
        }
      }
      const C = t => new x("string" == typeof t ? t : t + "", w),
        $ = (t, ...e) => {
          const n =
            1 === t.length
              ? t[0]
              : e.reduce(
                  (e, n, r) =>
                    e +
                    (t => {
                      if (!0 === t._$cssResult$) return t.cssText;
                      if ("number" == typeof t) return t;
                      throw Error(
                        "Value passed to 'css' function must be a 'css' function result: " +
                          t +
                          ". Use 'unsafeCSS' to pass non-literal values, but take care to ensure page security."
                      );
                    })(n) +
                    t[r + 1],
                  t[0]
                );
          return new x(n, w);
        },
        A = b
          ? t => t
          : t =>
              t instanceof CSSStyleSheet
                ? (t => {
                    let e = "";
                    for (const n of t.cssRules) e += n.cssText;
                    return C(e);
                  })(t)
                : t;
      var S;
      const k = window.trustedTypes,
        E = k ? k.emptyScript : "",
        W = window.reactiveElementPolyfillSupport,
        O = {
          toAttribute(t, e) {
            switch (e) {
              case Boolean:
                t = t ? E : null;
                break;
              case Object:
              case Array:
                t = null == t ? t : JSON.stringify(t);
            }
            return t;
          },
          fromAttribute(t, e) {
            let n = t;
            switch (e) {
              case Boolean:
                n = null !== t;
                break;
              case Number:
                n = null === t ? null : Number(t);
                break;
              case Object:
              case Array:
                try {
                  n = JSON.parse(t);
                } catch (t) {
                  n = null;
                }
            }
            return n;
          },
        },
        M = (t, e) => e !== t && (e == e || t == t),
        L = { attribute: !0, type: String, converter: O, reflect: !1, hasChanged: M };
      class T extends HTMLElement {
        constructor() {
          super(),
            (this._$Et = new Map()),
            (this.isUpdatePending = !1),
            (this.hasUpdated = !1),
            (this._$Ei = null),
            this.o();
        }
        static addInitializer(t) {
          var e;
          (null !== (e = this.l) && void 0 !== e) || (this.l = []), this.l.push(t);
        }
        static get observedAttributes() {
          this.finalize();
          const t = [];
          return (
            this.elementProperties.forEach((e, n) => {
              const r = this._$Eh(n, e);
              void 0 !== r && (this._$Eu.set(r, n), t.push(r));
            }),
            t
          );
        }
        static createProperty(t, e = L) {
          if (
            (e.state && (e.attribute = !1),
            this.finalize(),
            this.elementProperties.set(t, e),
            !e.noAccessor && !this.prototype.hasOwnProperty(t))
          ) {
            const n = "symbol" == typeof t ? Symbol() : "__" + t,
              r = this.getPropertyDescriptor(t, n, e);
            void 0 !== r && Object.defineProperty(this.prototype, t, r);
          }
        }
        static getPropertyDescriptor(t, e, n) {
          return {
            get() {
              return this[e];
            },
            set(r) {
              const i = this[t];
              (this[e] = r), this.requestUpdate(t, i, n);
            },
            configurable: !0,
            enumerable: !0,
          };
        }
        static getPropertyOptions(t) {
          return this.elementProperties.get(t) || L;
        }
        static finalize() {
          if (this.hasOwnProperty("finalized")) return !1;
          this.finalized = !0;
          const t = Object.getPrototypeOf(this);
          if (
            (t.finalize(),
            (this.elementProperties = new Map(t.elementProperties)),
            (this._$Eu = new Map()),
            this.hasOwnProperty("properties"))
          ) {
            const t = this.properties,
              e = [...Object.getOwnPropertyNames(t), ...Object.getOwnPropertySymbols(t)];
            for (const n of e) this.createProperty(n, t[n]);
          }
          return (this.elementStyles = this.finalizeStyles(this.styles)), !0;
        }
        static finalizeStyles(t) {
          const e = [];
          if (Array.isArray(t)) {
            const n = new Set(t.flat(1 / 0).reverse());
            for (const t of n) e.unshift(A(t));
          } else void 0 !== t && e.push(A(t));
          return e;
        }
        static _$Eh(t, e) {
          const n = e.attribute;
          return !1 === n ? void 0 : "string" == typeof n ? n : "string" == typeof t ? t.toLowerCase() : void 0;
        }
        o() {
          var t;
          (this._$Ep = new Promise(t => (this.enableUpdating = t))),
            (this._$AL = new Map()),
            this._$Em(),
            this.requestUpdate(),
            null === (t = this.constructor.l) || void 0 === t || t.forEach(t => t(this));
        }
        addController(t) {
          var e, n;
          (null !== (e = this._$Eg) && void 0 !== e ? e : (this._$Eg = [])).push(t),
            void 0 !== this.renderRoot &&
              this.isConnected &&
              (null === (n = t.hostConnected) || void 0 === n || n.call(t));
        }
        removeController(t) {
          var e;
          null === (e = this._$Eg) || void 0 === e || e.splice(this._$Eg.indexOf(t) >>> 0, 1);
        }
        _$Em() {
          this.constructor.elementProperties.forEach((t, e) => {
            this.hasOwnProperty(e) && (this._$Et.set(e, this[e]), delete this[e]);
          });
        }
        createRenderRoot() {
          var t;
          const e =
            null !== (t = this.shadowRoot) && void 0 !== t ? t : this.attachShadow(this.constructor.shadowRootOptions);
          return (
            ((t, e) => {
              b
                ? (t.adoptedStyleSheets = e.map(t => (t instanceof CSSStyleSheet ? t : t.styleSheet)))
                : e.forEach(e => {
                    const n = document.createElement("style"),
                      r = window.litNonce;
                    void 0 !== r && n.setAttribute("nonce", r), (n.textContent = e.cssText), t.appendChild(n);
                  });
            })(e, this.constructor.elementStyles),
            e
          );
        }
        connectedCallback() {
          var t;
          void 0 === this.renderRoot && (this.renderRoot = this.createRenderRoot()),
            this.enableUpdating(!0),
            null === (t = this._$Eg) ||
              void 0 === t ||
              t.forEach(t => {
                var e;
                return null === (e = t.hostConnected) || void 0 === e ? void 0 : e.call(t);
              });
        }
        enableUpdating(t) {}
        disconnectedCallback() {
          var t;
          null === (t = this._$Eg) ||
            void 0 === t ||
            t.forEach(t => {
              var e;
              return null === (e = t.hostDisconnected) || void 0 === e ? void 0 : e.call(t);
            });
        }
        attributeChangedCallback(t, e, n) {
          this._$AK(t, n);
        }
        _$ES(t, e, n = L) {
          var r, i;
          const o = this.constructor._$Eh(t, n);
          if (void 0 !== o && !0 === n.reflect) {
            const a = (
              null !== (i = null === (r = n.converter) || void 0 === r ? void 0 : r.toAttribute) && void 0 !== i
                ? i
                : O.toAttribute
            )(e, n.type);
            (this._$Ei = t), null == a ? this.removeAttribute(o) : this.setAttribute(o, a), (this._$Ei = null);
          }
        }
        _$AK(t, e) {
          var n, r, i;
          const o = this.constructor,
            a = o._$Eu.get(t);
          if (void 0 !== a && this._$Ei !== a) {
            const t = o.getPropertyOptions(a),
              s = t.converter,
              c =
                null !==
                  (i =
                    null !== (r = null === (n = s) || void 0 === n ? void 0 : n.fromAttribute) && void 0 !== r
                      ? r
                      : "function" == typeof s
                      ? s
                      : null) && void 0 !== i
                  ? i
                  : O.fromAttribute;
            (this._$Ei = a), (this[a] = c(e, t.type)), (this._$Ei = null);
          }
        }
        requestUpdate(t, e, n) {
          let r = !0;
          void 0 !== t &&
            (((n = n || this.constructor.getPropertyOptions(t)).hasChanged || M)(this[t], e)
              ? (this._$AL.has(t) || this._$AL.set(t, e),
                !0 === n.reflect &&
                  this._$Ei !== t &&
                  (void 0 === this._$EC && (this._$EC = new Map()), this._$EC.set(t, n)))
              : (r = !1)),
            !this.isUpdatePending && r && (this._$Ep = this._$E_());
        }
        async _$E_() {
          this.isUpdatePending = !0;
          try {
            await this._$Ep;
          } catch (t) {
            Promise.reject(t);
          }
          const t = this.scheduleUpdate();
          return null != t && (await t), !this.isUpdatePending;
        }
        scheduleUpdate() {
          return this.performUpdate();
        }
        performUpdate() {
          var t;
          if (!this.isUpdatePending) return;
          this.hasUpdated, this._$Et && (this._$Et.forEach((t, e) => (this[e] = t)), (this._$Et = void 0));
          let e = !1;
          const n = this._$AL;
          try {
            (e = this.shouldUpdate(n)),
              e
                ? (this.willUpdate(n),
                  null === (t = this._$Eg) ||
                    void 0 === t ||
                    t.forEach(t => {
                      var e;
                      return null === (e = t.hostUpdate) || void 0 === e ? void 0 : e.call(t);
                    }),
                  this.update(n))
                : this._$EU();
          } catch (t) {
            throw ((e = !1), this._$EU(), t);
          }
          e && this._$AE(n);
        }
        willUpdate(t) {}
        _$AE(t) {
          var e;
          null === (e = this._$Eg) ||
            void 0 === e ||
            e.forEach(t => {
              var e;
              return null === (e = t.hostUpdated) || void 0 === e ? void 0 : e.call(t);
            }),
            this.hasUpdated || ((this.hasUpdated = !0), this.firstUpdated(t)),
            this.updated(t);
        }
        _$EU() {
          (this._$AL = new Map()), (this.isUpdatePending = !1);
        }
        get updateComplete() {
          return this.getUpdateComplete();
        }
        getUpdateComplete() {
          return this._$Ep;
        }
        shouldUpdate(t) {
          return !0;
        }
        update(t) {
          void 0 !== this._$EC && (this._$EC.forEach((t, e) => this._$ES(e, this[e], t)), (this._$EC = void 0)),
            this._$EU();
        }
        updated(t) {}
        firstUpdated(t) {}
      }
      var I;
      (T.finalized = !0),
        (T.elementProperties = new Map()),
        (T.elementStyles = []),
        (T.shadowRootOptions = { mode: "open" }),
        null == W || W({ ReactiveElement: T }),
        (null !== (S = globalThis.reactiveElementVersions) && void 0 !== S
          ? S
          : (globalThis.reactiveElementVersions = [])
        ).push("1.3.1");
      const B = globalThis.trustedTypes,
        z = B ? B.createPolicy("lit-html", { createHTML: t => t }) : void 0,
        j = `lit$${(Math.random() + "").slice(9)}$`,
        P = "?" + j,
        U = `<${P}>`,
        R = document,
        H = (t = "") => R.createComment(t),
        N = t => null === t || ("object" != typeof t && "function" != typeof t),
        D = Array.isArray,
        q = t => {
          var e;
          return D(t) || "function" == typeof (null === (e = t) || void 0 === e ? void 0 : e[Symbol.iterator]);
        },
        F = /<(?:(!--|\/[^a-zA-Z])|(\/?[a-zA-Z][^>\s]*)|(\/?$))/g,
        Z = /-->/g,
        Q = />/g,
        Y = />|[ 	\n\r](?:([^\s"'>=/]+)([ 	\n\r]*=[ 	\n\r]*(?:[^ 	\n\r"'`<>=]|("|')|))|$)/g,
        V = /'/g,
        X = /"/g,
        G = /^(?:script|style|textarea|title)$/i,
        J =
          t =>
          (e, ...n) => ({ _$litType$: t, strings: e, values: n }),
        K = J(1),
        tt = (J(2), Symbol.for("lit-noChange")),
        et = Symbol.for("lit-nothing"),
        nt = new WeakMap(),
        rt = (t, e, n) => {
          var r, i;
          const o = null !== (r = null == n ? void 0 : n.renderBefore) && void 0 !== r ? r : e;
          let a = o._$litPart$;
          if (void 0 === a) {
            const t = null !== (i = null == n ? void 0 : n.renderBefore) && void 0 !== i ? i : null;
            o._$litPart$ = a = new ut(e.insertBefore(H(), t), t, void 0, null != n ? n : {});
          }
          return a._$AI(t), a;
        },
        it = R.createTreeWalker(R, 129, null, !1),
        ot = (t, e) => {
          const n = t.length - 1,
            r = [];
          let i,
            o = 2 === e ? "<svg>" : "",
            a = F;
          for (let e = 0; e < n; e++) {
            const n = t[e];
            let s,
              c,
              u = -1,
              l = 0;
            for (; l < n.length && ((a.lastIndex = l), (c = a.exec(n)), null !== c); )
              (l = a.lastIndex),
                a === F
                  ? "!--" === c[1]
                    ? (a = Z)
                    : void 0 !== c[1]
                    ? (a = Q)
                    : void 0 !== c[2]
                    ? (G.test(c[2]) && (i = RegExp("</" + c[2], "g")), (a = Y))
                    : void 0 !== c[3] && (a = Y)
                  : a === Y
                  ? ">" === c[0]
                    ? ((a = null != i ? i : F), (u = -1))
                    : void 0 === c[1]
                    ? (u = -2)
                    : ((u = a.lastIndex - c[2].length), (s = c[1]), (a = void 0 === c[3] ? Y : '"' === c[3] ? X : V))
                  : a === X || a === V
                  ? (a = Y)
                  : a === Z || a === Q
                  ? (a = F)
                  : ((a = Y), (i = void 0));
            const p = a === Y && t[e + 1].startsWith("/>") ? " " : "";
            o +=
              a === F
                ? n + U
                : u >= 0
                ? (r.push(s), n.slice(0, u) + "$lit$" + n.slice(u) + j + p)
                : n + j + (-2 === u ? (r.push(void 0), e) : p);
          }
          const s = o + (t[n] || "<?>") + (2 === e ? "</svg>" : "");
          if (!Array.isArray(t) || !t.hasOwnProperty("raw")) throw Error("invalid template strings array");
          return [void 0 !== z ? z.createHTML(s) : s, r];
        };
      class at {
        constructor({ strings: t, _$litType$: e }, n) {
          let r;
          this.parts = [];
          let i = 0,
            o = 0;
          const a = t.length - 1,
            s = this.parts,
            [c, u] = ot(t, e);
          if (((this.el = at.createElement(c, n)), (it.currentNode = this.el.content), 2 === e)) {
            const t = this.el.content,
              e = t.firstChild;
            e.remove(), t.append(...e.childNodes);
          }
          for (; null !== (r = it.nextNode()) && s.length < a; ) {
            if (1 === r.nodeType) {
              if (r.hasAttributes()) {
                const t = [];
                for (const e of r.getAttributeNames())
                  if (e.endsWith("$lit$") || e.startsWith(j)) {
                    const n = u[o++];
                    if ((t.push(e), void 0 !== n)) {
                      const t = r.getAttribute(n.toLowerCase() + "$lit$").split(j),
                        e = /([.?@])?(.*)/.exec(n);
                      s.push({
                        type: 1,
                        index: i,
                        name: e[2],
                        strings: t,
                        ctor: "." === e[1] ? pt : "?" === e[1] ? ft : "@" === e[1] ? ht : lt,
                      });
                    } else s.push({ type: 6, index: i });
                  }
                for (const e of t) r.removeAttribute(e);
              }
              if (G.test(r.tagName)) {
                const t = r.textContent.split(j),
                  e = t.length - 1;
                if (e > 0) {
                  r.textContent = B ? B.emptyScript : "";
                  for (let n = 0; n < e; n++) r.append(t[n], H()), it.nextNode(), s.push({ type: 2, index: ++i });
                  r.append(t[e], H());
                }
              }
            } else if (8 === r.nodeType)
              if (r.data === P) s.push({ type: 2, index: i });
              else {
                let t = -1;
                for (; -1 !== (t = r.data.indexOf(j, t + 1)); ) s.push({ type: 7, index: i }), (t += j.length - 1);
              }
            i++;
          }
        }
        static createElement(t, e) {
          const n = R.createElement("template");
          return (n.innerHTML = t), n;
        }
      }
      function st(t, e, n = t, r) {
        var i, o, a, s;
        if (e === tt) return e;
        let c = void 0 !== r ? (null === (i = n._$Cl) || void 0 === i ? void 0 : i[r]) : n._$Cu;
        const u = N(e) ? void 0 : e._$litDirective$;
        return (
          (null == c ? void 0 : c.constructor) !== u &&
            (null === (o = null == c ? void 0 : c._$AO) || void 0 === o || o.call(c, !1),
            void 0 === u ? (c = void 0) : ((c = new u(t)), c._$AT(t, n, r)),
            void 0 !== r ? ((null !== (a = (s = n)._$Cl) && void 0 !== a ? a : (s._$Cl = []))[r] = c) : (n._$Cu = c)),
          void 0 !== c && (e = st(t, c._$AS(t, e.values), c, r)),
          e
        );
      }
      class ct {
        constructor(t, e) {
          (this.v = []), (this._$AN = void 0), (this._$AD = t), (this._$AM = e);
        }
        get parentNode() {
          return this._$AM.parentNode;
        }
        get _$AU() {
          return this._$AM._$AU;
        }
        p(t) {
          var e;
          const {
              el: { content: n },
              parts: r,
            } = this._$AD,
            i = (null !== (e = null == t ? void 0 : t.creationScope) && void 0 !== e ? e : R).importNode(n, !0);
          it.currentNode = i;
          let o = it.nextNode(),
            a = 0,
            s = 0,
            c = r[0];
          for (; void 0 !== c; ) {
            if (a === c.index) {
              let e;
              2 === c.type
                ? (e = new ut(o, o.nextSibling, this, t))
                : 1 === c.type
                ? (e = new c.ctor(o, c.name, c.strings, this, t))
                : 6 === c.type && (e = new vt(o, this, t)),
                this.v.push(e),
                (c = r[++s]);
            }
            a !== (null == c ? void 0 : c.index) && ((o = it.nextNode()), a++);
          }
          return i;
        }
        m(t) {
          let e = 0;
          for (const n of this.v)
            void 0 !== n && (void 0 !== n.strings ? (n._$AI(t, n, e), (e += n.strings.length - 2)) : n._$AI(t[e])), e++;
        }
      }
      class ut {
        constructor(t, e, n, r) {
          var i;
          (this.type = 2),
            (this._$AH = et),
            (this._$AN = void 0),
            (this._$AA = t),
            (this._$AB = e),
            (this._$AM = n),
            (this.options = r),
            (this._$Cg = null === (i = null == r ? void 0 : r.isConnected) || void 0 === i || i);
        }
        get _$AU() {
          var t, e;
          return null !== (e = null === (t = this._$AM) || void 0 === t ? void 0 : t._$AU) && void 0 !== e
            ? e
            : this._$Cg;
        }
        get parentNode() {
          let t = this._$AA.parentNode;
          const e = this._$AM;
          return void 0 !== e && 11 === t.nodeType && (t = e.parentNode), t;
        }
        get startNode() {
          return this._$AA;
        }
        get endNode() {
          return this._$AB;
        }
        _$AI(t, e = this) {
          (t = st(this, t, e)),
            N(t)
              ? t === et || null == t || "" === t
                ? (this._$AH !== et && this._$AR(), (this._$AH = et))
                : t !== this._$AH && t !== tt && this.$(t)
              : void 0 !== t._$litType$
              ? this.T(t)
              : void 0 !== t.nodeType
              ? this.k(t)
              : q(t)
              ? this.S(t)
              : this.$(t);
        }
        M(t, e = this._$AB) {
          return this._$AA.parentNode.insertBefore(t, e);
        }
        k(t) {
          this._$AH !== t && (this._$AR(), (this._$AH = this.M(t)));
        }
        $(t) {
          this._$AH !== et && N(this._$AH) ? (this._$AA.nextSibling.data = t) : this.k(R.createTextNode(t)),
            (this._$AH = t);
        }
        T(t) {
          var e;
          const { values: n, _$litType$: r } = t,
            i =
              "number" == typeof r
                ? this._$AC(t)
                : (void 0 === r.el && (r.el = at.createElement(r.h, this.options)), r);
          if ((null === (e = this._$AH) || void 0 === e ? void 0 : e._$AD) === i) this._$AH.m(n);
          else {
            const t = new ct(i, this),
              e = t.p(this.options);
            t.m(n), this.k(e), (this._$AH = t);
          }
        }
        _$AC(t) {
          let e = nt.get(t.strings);
          return void 0 === e && nt.set(t.strings, (e = new at(t))), e;
        }
        S(t) {
          D(this._$AH) || ((this._$AH = []), this._$AR());
          const e = this._$AH;
          let n,
            r = 0;
          for (const i of t)
            r === e.length ? e.push((n = new ut(this.M(H()), this.M(H()), this, this.options))) : (n = e[r]),
              n._$AI(i),
              r++;
          r < e.length && (this._$AR(n && n._$AB.nextSibling, r), (e.length = r));
        }
        _$AR(t = this._$AA.nextSibling, e) {
          var n;
          for (null === (n = this._$AP) || void 0 === n || n.call(this, !1, !0, e); t && t !== this._$AB; ) {
            const e = t.nextSibling;
            t.remove(), (t = e);
          }
        }
        setConnected(t) {
          var e;
          void 0 === this._$AM && ((this._$Cg = t), null === (e = this._$AP) || void 0 === e || e.call(this, t));
        }
      }
      class lt {
        constructor(t, e, n, r, i) {
          (this.type = 1),
            (this._$AH = et),
            (this._$AN = void 0),
            (this.element = t),
            (this.name = e),
            (this._$AM = r),
            (this.options = i),
            n.length > 2 || "" !== n[0] || "" !== n[1]
              ? ((this._$AH = Array(n.length - 1).fill(new String())), (this.strings = n))
              : (this._$AH = et);
        }
        get tagName() {
          return this.element.tagName;
        }
        get _$AU() {
          return this._$AM._$AU;
        }
        _$AI(t, e = this, n, r) {
          const i = this.strings;
          let o = !1;
          if (void 0 === i) (t = st(this, t, e, 0)), (o = !N(t) || (t !== this._$AH && t !== tt)), o && (this._$AH = t);
          else {
            const r = t;
            let a, s;
            for (t = i[0], a = 0; a < i.length - 1; a++)
              (s = st(this, r[n + a], e, a)),
                s === tt && (s = this._$AH[a]),
                o || (o = !N(s) || s !== this._$AH[a]),
                s === et ? (t = et) : t !== et && (t += (null != s ? s : "") + i[a + 1]),
                (this._$AH[a] = s);
          }
          o && !r && this.C(t);
        }
        C(t) {
          t === et ? this.element.removeAttribute(this.name) : this.element.setAttribute(this.name, null != t ? t : "");
        }
      }
      class pt extends lt {
        constructor() {
          super(...arguments), (this.type = 3);
        }
        C(t) {
          this.element[this.name] = t === et ? void 0 : t;
        }
      }
      const dt = B ? B.emptyScript : "";
      class ft extends lt {
        constructor() {
          super(...arguments), (this.type = 4);
        }
        C(t) {
          t && t !== et ? this.element.setAttribute(this.name, dt) : this.element.removeAttribute(this.name);
        }
      }
      class ht extends lt {
        constructor(t, e, n, r, i) {
          super(t, e, n, r, i), (this.type = 5);
        }
        _$AI(t, e = this) {
          var n;
          if ((t = null !== (n = st(this, t, e, 0)) && void 0 !== n ? n : et) === tt) return;
          const r = this._$AH,
            i = (t === et && r !== et) || t.capture !== r.capture || t.once !== r.once || t.passive !== r.passive,
            o = t !== et && (r === et || i);
          i && this.element.removeEventListener(this.name, this, r),
            o && this.element.addEventListener(this.name, this, t),
            (this._$AH = t);
        }
        handleEvent(t) {
          var e, n;
          "function" == typeof this._$AH
            ? this._$AH.call(
                null !== (n = null === (e = this.options) || void 0 === e ? void 0 : e.host) && void 0 !== n
                  ? n
                  : this.element,
                t
              )
            : this._$AH.handleEvent(t);
        }
      }
      class vt {
        constructor(t, e, n) {
          (this.element = t), (this.type = 6), (this._$AN = void 0), (this._$AM = e), (this.options = n);
        }
        get _$AU() {
          return this._$AM._$AU;
        }
        _$AI(t) {
          st(this, t);
        }
      }
      const gt = window.litHtmlPolyfillSupport;
      var yt, mt;
      null == gt || gt(at, ut),
        (null !== (I = globalThis.litHtmlVersions) && void 0 !== I ? I : (globalThis.litHtmlVersions = [])).push(
          "2.2.2"
        );
      class bt extends T {
        constructor() {
          super(...arguments), (this.renderOptions = { host: this }), (this._$Dt = void 0);
        }
        createRenderRoot() {
          var t, e;
          const n = super.createRenderRoot();
          return (
            (null !== (t = (e = this.renderOptions).renderBefore) && void 0 !== t) || (e.renderBefore = n.firstChild), n
          );
        }
        update(t) {
          const e = this.render();
          this.hasUpdated || (this.renderOptions.isConnected = this.isConnected),
            super.update(t),
            (this._$Dt = rt(e, this.renderRoot, this.renderOptions));
        }
        connectedCallback() {
          var t;
          super.connectedCallback(), null === (t = this._$Dt) || void 0 === t || t.setConnected(!0);
        }
        disconnectedCallback() {
          var t;
          super.disconnectedCallback(), null === (t = this._$Dt) || void 0 === t || t.setConnected(!1);
        }
        render() {
          return tt;
        }
      }
      (bt.finalized = !0),
        (bt._$litElement$ = !0),
        null === (yt = globalThis.litElementHydrateSupport) || void 0 === yt || yt.call(globalThis, { LitElement: bt });
      const wt = globalThis.litElementPolyfillSupport;
      null == wt || wt({ LitElement: bt });
      (null !== (mt = globalThis.litElementVersions) && void 0 !== mt ? mt : (globalThis.litElementVersions = [])).push(
        "3.2.0"
      );
      var _t = n(86);
      function xt(t, e, n) {
        return t ? e() : null == n ? void 0 : n();
      }
      const Ct = 1,
        $t =
          t =>
          (...e) => ({ _$litDirective$: t, values: e });
      class At {
        constructor(t) {}
        get _$AU() {
          return this._$AM._$AU;
        }
        _$AT(t, e, n) {
          (this._$Ct = t), (this._$AM = e), (this._$Ci = n);
        }
        _$AS(t, e) {
          return this.update(t, e);
        }
        update(t, e) {
          return this.render(...e);
        }
      }
      const St = $t(
          class extends At {
            constructor(t) {
              var e;
              if (
                (super(t),
                t.type !== Ct ||
                  "class" !== t.name ||
                  (null === (e = t.strings) || void 0 === e ? void 0 : e.length) > 2)
              )
                throw Error(
                  "`classMap()` can only be used in the `class` attribute and must be the only part in the attribute."
                );
            }
            render(t) {
              return (
                " " +
                Object.keys(t)
                  .filter(e => t[e])
                  .join(" ") +
                " "
              );
            }
            update(t, [e]) {
              var n, r;
              if (void 0 === this.et) {
                (this.et = new Set()),
                  void 0 !== t.strings &&
                    (this.st = new Set(
                      t.strings
                        .join(" ")
                        .split(/\s/)
                        .filter(t => "" !== t)
                    ));
                for (const t in e)
                  e[t] && !(null === (n = this.st) || void 0 === n ? void 0 : n.has(t)) && this.et.add(t);
                return this.render(e);
              }
              const i = t.element.classList;
              this.et.forEach(t => {
                t in e || (i.remove(t), this.et.delete(t));
              });
              for (const t in e) {
                const n = !!e[t];
                n === this.et.has(t) ||
                  (null === (r = this.st) || void 0 === r ? void 0 : r.has(t)) ||
                  (n ? (i.add(t), this.et.add(t)) : (i.remove(t), this.et.delete(t)));
              }
              return tt;
            }
          }
        ),
        kt = $t(
          class extends At {
            constructor(t) {
              var e;
              if (
                (super(t),
                t.type !== Ct ||
                  "style" !== t.name ||
                  (null === (e = t.strings) || void 0 === e ? void 0 : e.length) > 2)
              )
                throw Error(
                  "The `styleMap` directive must be used in the `style` attribute and must be the only part in the attribute."
                );
            }
            render(t) {
              return Object.keys(t).reduce((e, n) => {
                const r = t[n];
                return null == r
                  ? e
                  : e + `${(n = n.replace(/(?:^(webkit|moz|ms|o)|)(?=[A-Z])/g, "-$&").toLowerCase())}:${r};`;
              }, "");
            }
            update(t, [e]) {
              const { style: n } = t.element;
              if (void 0 === this.ct) {
                this.ct = new Set();
                for (const t in e) this.ct.add(t);
                return this.render(e);
              }
              this.ct.forEach(t => {
                null == e[t] && (this.ct.delete(t), t.includes("-") ? n.removeProperty(t) : (n[t] = ""));
              });
              for (const t in e) {
                const r = e[t];
                null != r && (this.ct.add(t), t.includes("-") ? n.setProperty(t, r) : (n[t] = r));
              }
              return tt;
            }
          }
        );
      function Et(t) {
        return (
          (Et =
            "function" == typeof Symbol && "symbol" == typeof Symbol.iterator
              ? function (t) {
                  return typeof t;
                }
              : function (t) {
                  return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype
                    ? "symbol"
                    : typeof t;
                }),
          Et(t)
        );
      }
      var Wt, Ot, Mt, Lt, Tt, It, Bt, zt, jt, Pt;
      function Ut(t, e) {
        return e || (e = t.slice(0)), Object.freeze(Object.defineProperties(t, { raw: { value: Object.freeze(e) } }));
      }
      function Rt(t, e) {
        for (var n = 0; n < e.length; n++) {
          var r = e[n];
          (r.enumerable = r.enumerable || !1),
            (r.configurable = !0),
            "value" in r && (r.writable = !0),
            Object.defineProperty(t, r.key, r);
        }
      }
      function Ht(t, e) {
        return (
          (Ht =
            Object.setPrototypeOf ||
            function (t, e) {
              return (t.__proto__ = e), t;
            }),
          Ht(t, e)
        );
      }
      function Nt(t) {
        var e = (function () {
          if ("undefined" == typeof Reflect || !Reflect.construct) return !1;
          if (Reflect.construct.sham) return !1;
          if ("function" == typeof Proxy) return !0;
          try {
            return Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})), !0;
          } catch (t) {
            return !1;
          }
        })();
        return function () {
          var n,
            r = qt(t);
          if (e) {
            var i = qt(this).constructor;
            n = Reflect.construct(r, arguments, i);
          } else n = r.apply(this, arguments);
          return Dt(this, n);
        };
      }
      function Dt(t, e) {
        if (e && ("object" === Et(e) || "function" == typeof e)) return e;
        if (void 0 !== e) throw new TypeError("Derived constructors may only return object or undefined");
        return (function (t) {
          if (void 0 === t) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
          return t;
        })(t);
      }
      function qt(t) {
        return (
          (qt = Object.setPrototypeOf
            ? Object.getPrototypeOf
            : function (t) {
                return t.__proto__ || Object.getPrototypeOf(t);
              }),
          qt(t)
        );
      }
      function Ft(t, e, n) {
        return (
          e in t
            ? Object.defineProperty(t, e, { value: n, enumerable: !0, configurable: !0, writable: !0 })
            : (t[e] = n),
          t
        );
      }
      var Zt,
        Qt,
        Yt,
        Vt,
        Xt,
        Gt,
        Jt,
        Kt,
        te,
        ee = (function (e) {
          !(function (t, e) {
            if ("function" != typeof e && null !== e)
              throw new TypeError("Super expression must either be null or a function");
            (t.prototype = Object.create(e && e.prototype, {
              constructor: { value: t, writable: !0, configurable: !0 },
            })),
              e && Ht(t, e);
          })(u, e);
          var n,
            r,
            s,
            c = Nt(u);
          function u() {
            return (
              (function (t, e) {
                if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
              })(this, u),
              c.call(this)
            );
          }
          return (
            (n = u),
            (r = [
              {
                key: "open",
                value: function () {
                  this._isOpen = !0;
                },
              },
              {
                key: "close",
                value: function () {
                  (this._isOpen = !1),
                    this.integrationsService.fireEvent("WP_modal_close"),
                    this.dispatchEvent(new CustomEvent("close"));
                },
              },
              {
                key: "getMessengerName",
                value: function (t, e) {
                  return t && e ? "мессенджерах" : t ? "WhatsApp" : "Telegram";
                },
              },
              {
                key: "getBtnLogo",
                value: function (t) {
                  return {
                    whatsapp: K(
                      Wt ||
                        (Wt = Ut([
                          '\n        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" class="btn-logo">\n          <path d="M18.3775 5.60665C16.6854 3.92663 14.4351 3.00094 12.0376 3C7.09763 3 3.07712 6.98699 3.07515 11.8877C3.07451 13.4542 3.4872 14.9833 4.27148 16.3311L3 20.937L7.75109 19.7011C9.06016 20.4091 10.534 20.7823 12.034 20.7829H12.0377C16.9771 20.7829 20.998 16.7954 21 11.8947C21.0009 9.51981 20.0696 7.28667 18.3775 5.60665ZM12.0377 19.2818H12.0346C10.698 19.2813 9.38694 18.9252 8.24323 18.2521L7.97119 18.092L5.15182 18.8254L5.90438 16.0993L5.72721 15.8198C4.98154 14.6436 4.58772 13.2841 4.5883 11.8882C4.58991 7.81503 7.93163 4.50116 12.0406 4.50116C14.0302 4.50191 15.9005 5.27138 17.307 6.66773C18.7134 8.06407 19.4875 9.92021 19.4868 11.8942C19.4852 15.9677 16.1435 19.2818 12.0377 19.2818ZM16.1237 13.7489C15.8997 13.6377 14.7988 13.1005 14.5935 13.0263C14.3882 12.9522 14.239 12.9152 14.0896 13.1375C13.9404 13.3598 13.5112 13.8601 13.3805 14.0083C13.2499 14.1565 13.1193 14.1751 12.8954 14.0639C12.6714 13.9527 11.9498 13.7182 11.0945 12.9616C10.4288 12.3728 9.97933 11.6454 9.84871 11.4231C9.71809 11.2007 9.83482 11.0806 9.94692 10.9698C10.0476 10.8703 10.1709 10.7104 10.2828 10.5807C10.3948 10.4511 10.4321 10.3584 10.5068 10.2102C10.5814 10.062 10.5441 9.93227 10.4881 9.82111C10.4321 9.70995 9.98424 8.61682 9.79763 8.17216C9.61587 7.7391 9.43121 7.79769 9.29376 7.79089C9.16328 7.78445 9.01384 7.78309 8.86457 7.78309C8.7153 7.78309 8.47267 7.83866 8.26741 8.061C8.06215 8.28334 7.48365 8.82064 7.48365 9.91375C7.48365 11.0069 8.28606 12.0629 8.39804 12.2111C8.51001 12.3594 9.97711 14.6026 12.2235 15.5646C12.7578 15.7934 13.175 15.93 13.5002 16.0324C14.0366 16.2014 14.5248 16.1776 14.9107 16.1204C15.341 16.0566 16.2356 15.5832 16.4223 15.0644C16.6089 14.5456 16.6089 14.1009 16.5529 14.0083C16.4969 13.9157 16.3476 13.8601 16.1237 13.7489Z" fill="white"/>\n        </svg>\n      ',
                        ]))
                    ),
                    telegram: K(
                      Ot ||
                        (Ot = Ut([
                          '\n        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" class="btn-logo">\n          <path d="M10.0629 14.3858L9.76513 18.5738C10.1911 18.5738 10.3756 18.3908 10.5969 18.1711L12.5941 16.2623L16.7326 19.2931C17.4916 19.7161 18.0264 19.4933 18.2311 18.5948L20.9476 5.86581L20.9484 5.86506C21.1891 4.74306 20.5426 4.30431 19.8031 4.57956L3.83563 10.6928C2.74588 11.1158 2.76238 11.7233 3.65038 11.9986L7.73263 13.2683L17.2149 7.33506C17.6611 7.03956 18.0669 7.20306 17.7331 7.49856L10.0629 14.3858Z" fill="white"/>\n        </svg>\n      ',
                        ]))
                    ),
                  }[t];
                },
              },
              {
                key: "getMessengerLogo",
                value: function (t, e) {
                  var n = { whatsapp: "Whatsapp", telegram: "Telegram" },
                    r = {
                      whatsapp: K(
                        Mt ||
                          (Mt = Ut([
                            '\n        <svg width="56" height="56" viewBox="0 0 56 56" fill="none" xmlns="http://www.w3.org/2000/svg">\n          <rect width="56" height="56" rx="28" fill="#02A83E"/>\n          <path d="M38.4166 17.5576C35.6529 14.8135 31.9774 13.3016 28.0614 13.3C19.9929 13.3 13.426 19.8121 13.4228 27.8166C13.4218 30.3752 14.0958 32.8728 15.3768 35.0743L13.3 42.5972L21.0602 40.5784C23.1983 41.735 25.6055 42.3444 28.0556 42.3455H28.0616C36.1293 42.3455 42.6968 35.8326 42.7 27.8281C42.7016 23.9491 41.1804 20.3016 38.4166 17.5576ZM28.0616 39.8936H28.0566C25.8734 39.8928 23.7321 39.3111 21.864 38.2117L21.4197 37.9502L16.8147 39.1482L18.0439 34.6956L17.7545 34.2391C16.5366 32.3179 15.8933 30.0974 15.8943 27.8175C15.8969 21.1646 21.355 15.7519 28.0663 15.7519C31.3161 15.7532 34.3709 17.01 36.6681 19.2907C38.9653 21.5714 40.2296 24.6031 40.2284 27.8272C40.2258 34.4807 34.7677 39.8936 28.0616 39.8936ZM34.7354 30.8566C34.3696 30.675 32.5714 29.7975 32.2361 29.6764C31.9008 29.5553 31.657 29.4948 31.4131 29.858C31.1693 30.2211 30.4683 31.0382 30.2549 31.2803C30.0416 31.5224 29.8282 31.5527 29.4625 31.3711C29.0967 31.1895 27.9181 30.8064 26.5211 29.5706C25.4338 28.6089 24.6996 27.4209 24.4863 27.0577C24.2729 26.6946 24.4636 26.4983 24.6467 26.3174C24.8112 26.1549 25.0124 25.8938 25.1953 25.6819C25.3782 25.4702 25.4392 25.3188 25.5611 25.0767C25.683 24.8346 25.622 24.6228 25.5306 24.4412C25.4392 24.2596 24.7076 22.4742 24.4028 21.7479C24.106 21.0406 23.8044 21.1363 23.5799 21.1252C23.3667 21.1146 23.1227 21.1124 22.8788 21.1124C22.635 21.1124 22.2387 21.2032 21.9035 21.5663C21.5682 21.9295 20.6233 22.8071 20.6233 24.5925C20.6233 26.3779 21.9339 28.1028 22.1168 28.3449C22.2997 28.587 24.696 32.251 28.3652 33.8222C29.2379 34.1959 29.9191 34.4191 30.4504 34.5863C31.3266 34.8623 32.1239 34.8234 32.7542 34.73C33.4569 34.6259 34.9182 33.8525 35.2231 33.0052C35.5279 32.1578 35.5279 31.4316 35.4364 31.2803C35.345 31.129 35.1012 31.0382 34.7354 30.8566Z" fill="white"/>\n        </svg>\n        <h4 class="pfModalQrCodeTitle">',
                            "</h4>\n      ",
                          ])),
                        n[t]
                      ),
                      telegram: K(
                        Lt ||
                          (Lt = Ut([
                            '\n        <svg width="56" height="56" viewBox="0 0 56 56" fill="none" xmlns="http://www.w3.org/2000/svg">\n          <rect width="56" height="56" rx="28" fill="#0099E5"/>\n          <path d="M24.8358 31.8967L24.3495 38.7371C25.0453 38.7371 25.3467 38.4382 25.708 38.0793L28.9702 34.9617L35.7298 39.9119C36.9695 40.6028 37.8429 40.239 38.1773 38.7714L42.6143 17.9807L42.6155 17.9795C43.0087 16.1469 41.9528 15.4303 40.7449 15.8799L14.6647 25.8648C12.8847 26.5557 12.9117 27.548 14.3621 27.9976L21.0298 30.0715L36.5174 20.3805C37.2463 19.8979 37.909 20.1649 37.3639 20.6476L24.8358 31.8967Z" fill="white"/>\n        </svg>\n        <h4 class="pfModalQrCodeTitle">',
                            "</h4>\n      ",
                          ])),
                        n[t]
                      ),
                    };
                  return e ? r[t] : "";
                },
              },
              {
                key: "render",
                value: function () {
                  var e = this,
                    n = this.widget,
                    r = this.wazzupId,
                    i = n.whatsappReady && this.widget.telegramReady;
                  return K(
                    Tt ||
                      (Tt = Ut([
                        '\n      <div\n    pseudo="--vidget-modal"    class="pfModalOverlay"\n        aria-hidden="',
                        '"\n        style="',
                        '"\n        @click="',
                        '"\n      >\n        <div class="pfModal" @click="',
                        '">\n          <button class="pfModalCloseBtn" @click="',
                        '">\n            <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">\n              <path d="M14 1.41L12.59 0L7 5.59L1.41 0L0 1.41L5.59 7L0 12.59L1.41 14L7 8.41L12.59 14L14 12.59L8.41 7L14 1.41Z" fill="#9C9C9C"/>\n            </svg>\n          </button>\n          \n          <h3 class="pfModalTitle">\n            ',
                        '\n          </h3>\n          \n          <div class="pfModalQrCodes">\n            ',
                        "\n              \n            ",
                        "\n          </div>\n  \n          ",
                        "\n        </div>\n      </div>\n    ",
                      ])),
                    !this._isOpen,
                    kt({ display: this._isOpen ? "block" : "none" }),
                    function (t) {
                      return e.close();
                    },
                    function (t) {
                      return t.stopPropagation();
                    },
                    this.close,
                    this.getTitle(),
                    xt(
                      this.widget.whatsappReady,
                      function () {
                        return e.qrCodeItemTemplate(
                          "whatsapp",
                          e.handleWhatsappOpenClick,
                          (function (e, n) {
                            var r = e.id,
                              i = e.whatsappNumber,
                              o = e.appendWazzupId,
                              a = e.whatsappGreetingMessage,
                              s = t(o, a, n);
                            return (
                              "".concat("https://widget.profeat.team", "/api/settings/whatsapp/qr") +
                              "?id=".concat(r) +
                              "&wid=".concat(n) +
                              "&p=".concat(encodeURIComponent(i)) +
                              "&m=".concat(encodeURIComponent(s))
                            );
                          })(n, r),
                          i
                        );
                      },
                      function () {
                        return et;
                      }
                    ),
                    xt(
                      this.widget.telegramReady,
                      function () {
                        return e.qrCodeItemTemplate(
                          "telegram",
                          e.handleTelegramOpenClick,
                          (function (t, e) {
                            return (
                              "".concat("https://widget.profeat.team", "/api/settings/telegram/qr") +
                              "?id=".concat(t.id) +
                              "&wid=".concat(e) +
                              "&p=".concat(t.telegramUsername)
                            );
                          })(n, r),
                          i
                        );
                      },
                      function () {
                        return et;
                      }
                    ),
                    xt(
                      this.widget.appearance.desktop.branding,
                      function () {
                        return K(
                          It ||
                            (It = Ut([
                              '\n              <div class="pfWidgetLinkBlock">\n                <a class="pfWidgetLink" href="',
                              '">\n                  <span>Сделано в</span>\n                  <span>Widget</span>\n                  <span>Profeat</span>\n                </a>\n              </div>\n            ',
                            ])),
                          e.getBrandingLink()
                        );
                      },
                      function () {
                        return et;
                      }
                    )
                  );
                },
              },
              {
                key: "getTitle",
                value: function () {
                  return this.widget.appearance.desktop.modal_title
                    ? this.widget.appearance.desktop.modal_title
                    : "Мы на связи в ".concat(
                        this.getMessengerName(this.widget.whatsappReady, this.widget.telegramReady)
                      );
                },
              },
              {
                key: "qrCodeItemTemplate",
                value: function (t, e, n, r) {
                  var o = this,
                    a = { whatsapp: "pfModalOrMobileWhatsapp", telegram: "pfModalOrMobileTelegram" },
                    s = r ? "Написать в ".concat({ whatsapp: "WhatsApp", telegram: "Telegram" }[t]) : "Написать";
                  return K(
                    Bt ||
                      (Bt = Ut([
                        '\n      <div class="pfModalQrCodeItem">\n        ',
                        '\n\n        <button\n          class="pfModalContinueBtn ',
                        " ",
                        '"\n          @click="',
                        '"\n        >\n          ',
                        "\n          <span>",
                        "</span>\n        </button>\n\n        ",
                        "\n\n        ",
                        "\n    </div>\n    ",
                      ])),
                    xt(
                      i(),
                      function () {
                        return et;
                      },
                      function () {
                        return o.getMessengerLogo(t, r);
                      }
                    ),
                    { whatsapp: "pfModalContinueBtnWhatsapp", telegram: "pfModalContinueBtnTelegram" }[t],
                    r && "pfWidgetBtnWithoutLogo",
                    e,
                    xt(
                      i(),
                      function () {
                        return o.getBtnLogo(t);
                      },
                      function () {
                        return et;
                      }
                    ),
                    s,
                    xt(
                      i(),
                      function () {
                        return et;
                      },
                      function () {
                        return K(
                          zt ||
                            (zt = Ut([
                              '\n            <p class="pfModalOrMobile ',
                              '">\n              или\n              <span>с телефона</span>\n\n              <svg class="pfModalOrMobileHint" width="20" height="68" viewBox="0 0 20 68" fill="none" xmlns="http://www.w3.org/2000/svg">\n                <path d="M2.80472 1.10981C2.31308 0.665381 1.55425 0.703647 1.10981 1.19528C0.665381 1.68692 0.703647 2.44575 1.19528 2.89019L2.80472 1.10981ZM0.803237 65.9119C0.754593 66.5729 1.25097 67.1481 1.91192 67.1968L12.6828 67.9895C13.3437 68.0381 13.919 67.5417 13.9676 66.8808C14.0163 66.2198 13.5199 65.6446 12.8589 65.5959L3.28484 64.8913L3.98946 55.3172C4.0381 54.6563 3.54173 54.081 2.88077 54.0324C2.21982 53.9837 1.64458 54.4801 1.59593 55.1411L0.803237 65.9119ZM1.19528 2.89019C7.09926 8.22731 14.1621 17.7551 16.214 28.9329C18.2466 40.0051 15.3974 52.8544 1.21604 65.0915L2.78396 66.9085C17.5801 54.1409 20.7679 40.4476 18.5746 28.4996C16.4007 16.6573 8.97472 6.68742 2.80472 1.10981L1.19528 2.89019Z" fill="currentColor"/>\n              </svg>\n            </p>\n            ',
                            ])),
                          a[t]
                        );
                      }
                    ),
                    xt(
                      i(),
                      function () {
                        return et;
                      },
                      function () {
                        return K(
                          jt ||
                            (jt = Ut([
                              '\n            <img\n              class="pfModalQrImg"\n              src="',
                              '"\n              alt="qrCode"\n            >\n          ',
                            ])),
                          n
                        );
                      }
                    )
                  );
                },
              },
              {
                key: "handleWhatsappOpenClick",
                value: function () {
                  var t = i() ? "WM".concat(this.wazzupId) : "WW".concat(this.wazzupId);
                  this.integrationsService.fireEvent("WP_click_whatsapp"), window.open(o(this.widget, t), "_blank");
                },
              },
              {
                key: "handleTelegramOpenClick",
                value: function () {
                  var t = i() ? "WM".concat(this.wazzupId) : "WW".concat(this.wazzupId);
                  this.integrationsService.fireEvent("WP_click_telegram"), window.open(a(this.widget, t), "_blank");
                },
              },
              {
                key: "getBrandingLink",
                value: function () {
                  return "https://widget.profeat.team?utm_source=branding&utm_campaign=".concat(
                    encodeURIComponent(window.location.host)
                  );
                },
              },
            ]),
            r && Rt(n.prototype, r),
            s && Rt(n, s),
            u
          );
        })(bt);
      function ne(t) {
        return (
          (ne =
            "function" == typeof Symbol && "symbol" == typeof Symbol.iterator
              ? function (t) {
                  return typeof t;
                }
              : function (t) {
                  return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype
                    ? "symbol"
                    : typeof t;
                }),
          ne(t)
        );
      }
      function re(t, e) {
        return e || (e = t.slice(0)), Object.freeze(Object.defineProperties(t, { raw: { value: Object.freeze(e) } }));
      }
      function ie(t, e) {
        for (var n = 0; n < e.length; n++) {
          var r = e[n];
          (r.enumerable = r.enumerable || !1),
            (r.configurable = !0),
            "value" in r && (r.writable = !0),
            Object.defineProperty(t, r.key, r);
        }
      }
      function oe() {
        return (
          (oe =
            "undefined" != typeof Reflect && Reflect.get
              ? Reflect.get
              : function (t, e, n) {
                  var r = ae(t, e);
                  if (r) {
                    var i = Object.getOwnPropertyDescriptor(r, e);
                    return i.get ? i.get.call(arguments.length < 3 ? t : n) : i.value;
                  }
                }),
          oe.apply(this, arguments)
        );
      }
      function ae(t, e) {
        for (; !Object.prototype.hasOwnProperty.call(t, e) && null !== (t = le(t)); );
        return t;
      }
      function se(t, e) {
        return (
          (se =
            Object.setPrototypeOf ||
            function (t, e) {
              return (t.__proto__ = e), t;
            }),
          se(t, e)
        );
      }
      function ce(t) {
        var e = (function () {
          if ("undefined" == typeof Reflect || !Reflect.construct) return !1;
          if (Reflect.construct.sham) return !1;
          if ("function" == typeof Proxy) return !0;
          try {
            return Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})), !0;
          } catch (t) {
            return !1;
          }
        })();
        return function () {
          var n,
            r = le(t);
          if (e) {
            var i = le(this).constructor;
            n = Reflect.construct(r, arguments, i);
          } else n = r.apply(this, arguments);
          return ue(this, n);
        };
      }
      function ue(t, e) {
        if (e && ("object" === ne(e) || "function" == typeof e)) return e;
        if (void 0 !== e) throw new TypeError("Derived constructors may only return object or undefined");
        return (function (t) {
          if (void 0 === t) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
          return t;
        })(t);
      }
      function le(t) {
        return (
          (le = Object.setPrototypeOf
            ? Object.getPrototypeOf
            : function (t) {
                return t.__proto__ || Object.getPrototypeOf(t);
              }),
          le(t)
        );
      }
      function pe(t, e, n) {
        return (
          e in t
            ? Object.defineProperty(t, e, { value: n, enumerable: !0, configurable: !0, writable: !0 })
            : (t[e] = n),
          t
        );
      }
      Ft(ee, "properties", {
        _isOpen: { type: Boolean },
        widget: { type: Object },
        wazzupId: { type: String },
        integrationsService: { type: Object },
      }),
        Ft(
          ee,
          "styles",
          $(
            Pt ||
              (Pt = Ut([
                "\n    .pfModalOverlay {\n      position: fixed;\n      top: 0;\n      left: 0;\n      right: 0;\n      bottom: 0;\n      background: rgba(66, 66, 66, 0.48);\n      z-index: 2147483647;\n    }\n\n    .pfModal {\n      position: fixed;\n      top: 50%;\n      left: 50%;\n      transform: translate(-50%, -50%);\n      width: 738px;\n      border-radius: 24px;\n      background: #fff;\n      padding: 64px 86px;\n      font-family: Ubuntu, sans-serif;\n      box-sizing: border-box;\n    }\n\n    @media (max-width: 960px) {\n      .pfModal {\n        width: 90%;\n        padding: 32px;\n      }\n    }\n\n    .pfModalCloseBtn {\n      position: absolute;\n      top: 16px;\n      right: 16px;\n      background: transparent;\n      border: none;\n      appearance: initial;\n      cursor: pointer;\n      width: 32px;\n      height: 32px;\n      padding: 0;\n\n      @media (max-width: 960px) {\n        top: 8px;\n        right: 8px;\n      }\n    }\n\n    .pfModalTitle {\n      color: #212121;\n      font-size: 28px;\n      text-align: center;\n      font-weight: 400;\n      margin-top: 0;\n      margin-bottom: 24px;\n    }\n\n    .pfModalQrCodes {\n      display: flex;\n    }\n\n    @media (min-width: 960px) {\n      .pfModalQrCodes {\n        justify-content: center;\n      }\n    }\n    @media (max-width: 960px) {\n      .pfModalQrCodes {\n        flex-direction: column;\n        align-items: center;\n      }\n    }\n\n    .pfModalQrCodeItem {\n      text-align: center;\n      width: 232px;\n    }\n    \n    .pfModalQrCodeItem:not(:last-child) .pfModalContinueBtn {\n      margin-bottom: 16px;\n    }\n\n    @media (min-width: 960px) {\n      .pfModalQrCodeItem:nth-child(2) {\n        margin-left: 64px;\n      }\n    }\n\n    .pfModalQrCodeTitle {\n      font-size: 20px;\n      font-weight: 500;\n      margin-top: 16px;\n    }\n\n    .pfModalContinueBtn {\n      align-items: center;\n      display: flex;\n      justify-content: center;\n      box-shadow: 0 6px 12px 0 #2121211a;\n      text-decoration: none;\n      border: none;\n      width: 100%;\n      padding: 12px;\n      font-size: 16px;\n      font-weight: 500;\n      border-radius: 8px;\n      cursor: pointer;\n    }\n\n    .pfModalContinueBtn svg {\n      margin-right: 8px;\n    }\n\n    .pfModalContinueBtnWhatsapp {\n      background: #02a83e;\n      color: #fff;\n    }\n\n    .pfModalContinueBtnTelegram {\n      background: #0099e5;\n      color: #fff;\n    }\n\n    .pfModalQrImg {\n      width: 170px;\n      height: 170px;\n    }\n\n    .pfModalOrMobile {\n      font-size: 20px;\n      font-weight: 500;\n      margin: 32px 0 16px;\n      position: relative;\n    }\n\n    .pfModalOrMobileWhatsapp svg path {\n      fill: #02a83e;\n    }\n    \n    .pfModalOrMobileWhatsapp span {\n      color: #02a83e;\n    }\n\n    .pfModalOrMobileTelegram svg path {\n      fill: #0099e5;\n    }\n    \n    .pfModalOrMobileTelegram span {\n      color: #0099e5;\n    }\n\n    .pfModalOrMobileHint {\n      position: absolute;\n      right: 0;\n      top: 50%;\n    }\n\n    .pfWidgetLinkBlock {\n      text-align: center;\n    }\n\n    .pfWidgetLink {\n      text-decoration: none;\n      display: inline-block;\n      padding: 4px 8px;\n      color: #bbbbbb;\n      border-radius: 6px;\n      border: 1px solid #e0e0e0;\n      font-size: 10px;\n      line-height: 14px;\n      margin-top: 24px;\n      transition: 0.2s ease-in-out;\n    }\n    \n    @media (max-width: 960px) {\n      margin-top: 16px;\n    }\n\n    .pfWidgetLink span:nth-child(2) {\n      background-color: #bbbbbb;\n      color: #fff;\n      padding: 2px 4px;\n      border-radius: 4px;\n      margin-left: 2px;\n      line-height: 1;\n    }\n\n    .pfWidgetLink span:nth-child(2),\n    .pfWidgetLink span:nth-child(3) {\n      font-weight: 700;\n    }\n\n    .pfWidgetLink:hover {\n      background-color: #F0F0F0;\n      border-color: #D0D0D0;\n    }\n\n    .pfWidgetLink:active,\n    .pfWidgetLink:focus {\n      border-color: #F0F0F0;\n      background-color: #F0F0F0;\n      color: #0eb64d;\n      span:nth-child(2) {\n        background-color: #0eb64d;\n      }\n    }\n\n    .pfWidgetBtnWithoutLogo {\n      justify-content: center;\n    }\n  ",
              ]))
          )
        ),
        customElements.define("pf-modal", ee);
      var de = (function (t) {
        !(function (t, e) {
          if ("function" != typeof e && null !== e)
            throw new TypeError("Super expression must either be null or a function");
          (t.prototype = Object.create(e && e.prototype, {
            constructor: { value: t, writable: !0, configurable: !0 },
          })),
            e && se(t, e);
        })(c, t);
        var e,
          n,
          r,
          s = ce(c);
        function c() {
          return (
            (function (t, e) {
              if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
            })(this, c),
            s.call(this)
          );
        }
        return (
          (e = c),
          (n = [
            {
              key: "openModal",
              value: function () {
                this.shadowRoot.querySelector("pf-modal").open();
              },
            },
            {
              key: "openModalWithMobile",
              value: function () {
                i() ? this.widget.whatsappReady && this.widget.telegramReady && (this._isOpen = !0) : this.openModal();
              },
            },
            {
              key: "closeModal",
              value: function () {
                this.shadowRoot.querySelector("pf-modal").close();
              },
            },
            {
              key: "handleClose",
              value: function () {
                this._isOpen = !this._isOpen;
              },
            },
            {
              key: "handleMixedOpen",
              value: function () {
                if ((this.integrationsService.fireEvent("WP_click_widget"), i()))
                  (this._animationDurationSpent = 0), (this._isOpen = !this._isOpen);
                else {
                  var t = i() ? "WM".concat(this.wazzupId) : "WW".concat(this.wazzupId);
                  l(this.widget.id, { name: "click", wazzupId: t }), this.openModal();
                }
              },
            },
            {
              key: "telegramClick",
              value: function () {
                var t = i() ? "WM".concat(this.wazzupId) : "WW".concat(this.wazzupId);
                l(this.widget.id, { name: "click", wazzupId: t }),
                  this.integrationsService.fireEvent("WP_click_telegram"),
                  i() ? window.open(a(this.widget, t), "_blank") : this.openModal();
              },
            },
            {
              key: "whatsappClick",
              value: function () {
                var t = i() ? "WM".concat(this.wazzupId) : "WW".concat(this.wazzupId);
                l(this.widget.id, { name: "click", wazzupId: t }),
                  this.integrationsService.fireEvent("WP_click_whatsapp"),
                  i() ? window.open(o(this.widget, t), "_blank") : this.openModal();
              },
            },
            {
              key: "connectedCallback",
              value: function () {
                var t = this;
                oe(le(c.prototype), "connectedCallback", this).call(this),
                  setInterval(function () {
                    var e;
                    (t._animationDurationSpent = null !== (e = t._animationDurationSpent) && void 0 !== e ? e : 0),
                      t._animationDurationSpent++;
                  }, 1e3),
                  setTimeout(function () {
                    t.anyActionPassed$.subscribe(function () {
                      t.openModal();
                    }),
                      t.modalStateChange$.subscribe(function (e) {
                        e ? t.openModal() : ((t._isOpen = !1), t.closeModal());
                      });
                  }, 0);
              },
            },
            {
              key: "render",
              value: function () {
                var t = this,
                  e = this.widget.whatsappReady && this.widget.telegramReady,
                  n = i() ? this.widget.appearance.mobile : this.widget.appearance.desktop,
                  r = n.size || "80",
                  o = i()
                    ? "mobile_".concat(this.widget.appearance.mobile.position)
                    : "desktop_".concat(this.widget.appearance.desktop.position);
                return K(
                  Zt ||
                    (Zt = re([
                      '\n      <pf-modal\n    class="modal"     .widget="',
                      '"\n        .wazzupId="',
                      '"\n        .integrationsService="',
                      '"\n      ></pf-modal>\n      ',
                      "\n    ",
                    ])),
                  this.widget,
                  this.wazzupId,
                  this.integrationsService,
                  xt(
                    this.widget.hidden,
                    function () {
                      return et;
                    },
                    function () {
                      return K(
                        Qt || (Qt = re([""])),
                        o,
                        kt({
                          margin: "".concat(n.gap_y, "px ").concat(n.gap_x, "px"),
                          width: "".concat(r, "px"),
                          height: "".concat(r, "px"),
                        }),
                        kt({ width: "".concat(r, "px"), height: "".concat(r, "px") }),
                        xt(
                          t.widget.whatsappReady,
                          function () {
                            return t.btnTemplate("whatsapp");
                          },
                          function () {
                            return et;
                          }
                        ),
                        xt(
                          t.widget.telegramReady,
                          function () {
                            return t.btnTemplate("telegram");
                          },
                          function () {
                            return et;
                          }
                        ),
                        xt(
                          e,
                          function () {
                            return t.mainBtnTemplate();
                          },
                          function () {
                            return et;
                          }
                        )
                      );
                    }
                  )
                );
              },
            },
            {
              key: "btnTemplate",
              value: function (t) {
                var e,
                  n = { whatsapp: this.whatsappClick, telegram: this.telegramClick },
                  r = {
                    whatsapp: K(Yt || (Yt = re(["\n        \n      "]))),
                    telegram: K(Vt || (Vt = re(["\n      \n      "]))),
                  },
                  i = this.widget.whatsappReady && this.widget.telegramReady;
                return K(
                  Xt ||
                    (Xt = re([
                      "\n      <div\n        class=",
                      '\n      >\n        <button class="pfWidgetBtn pfWidgetBtnWhatsapp" @click="',
                      '">\n          ',
                      '\n        </button>\n        <div class="pfWidgetBtnWave" style=',
                      '></div>\n        <div class="pfWidgetBtnBorder pfWidgetBtnBorderWhatsapp"></div>\n      </div>\n    ',
                    ])),
                  St(
                    (pe(
                      (e = { pfWidgetBtnWrap: !0 }),
                      { whatsapp: "pfWidgetBtnWrapWhatsapp", telegram: "pfWidgetBtnWrapTelegram" }[t],
                      !0
                    ),
                    pe(
                      e,
                      { whatsapp: "pfWidgetBtnWrapWhatsappActive", telegram: "pfWidgetBtnWrapTelegramActive" }[t],
                      this._isOpen
                    ),
                    e)
                  ),
                  n[t],
                  r[t],
                  kt({ visibility: this._isOpen || !i ? "visible" : "hidden" })
                );
              },
            },
            {
              key: "mainBtnTemplate",
              value: function () {
                var t = this,
                  e = this._animationDurationSpent % 8 == 0;
                return K(
                  Gt || (Gt = re(["\n      ", "\n    "])),
                  xt(
                    this._isOpen,
                    function () {
                      return K(
                        Jt ||
                          (Jt = re([
                            '\n          <div class="pfWidgetBtnWrap" style="visibility: visible;" @click="',
                            '">\n            <button class="pfWidgetBtn pfWidgetBtnOutline">\n              <svg class="pfWidgetIcon" width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">\n                <path d="M31.0334 10.4697L28.7304 8.16669L19.6001 17.297L10.4697 8.16669L8.16675 10.4697L17.2971 19.6L8.16675 28.7304L10.4697 31.0334L19.6001 21.903L28.7304 31.0334L31.0334 28.7304L21.9031 19.6L31.0334 10.4697Z" fill="#25D366"/>\n              </svg>\n            </button>\n            <div class="pfWidgetBtnBorder pfWidgetBtnBorderOutline"></div>\n          </div>\n        ',
                          ])),
                        t.handleClose
                      );
                    },
                    function () {
                      return K(
                        Kt ||
                          (Kt = re([
                            '\n          <div class="pfWidgetBtnWrap pfWidgetBtnWrapMixed" style="visibility: visible;" @click="',
                            '">\n            <button class="pfWidgetBtn">\n              <img src="',
                            '" alt="Mixed logo">\n            </button>\n            <div class="pfWidgetBtnWave ',
                            '"></div>\n            <div class="pfWidgetBtnBorder"></div>\n          </div>\n        ',
                          ])),
                        t.handleMixedOpen,
                        _t,
                        e ? "" : "pfWidgetBtnWaveMixed"
                      );
                    }
                  )
                );
              },
            },
          ]) && ie(e.prototype, n),
          r && ie(e, r),
          c
        );
      })(bt);
      function fe(t, e) {
        var n = ("undefined" != typeof Symbol && t[Symbol.iterator]) || t["@@iterator"];
        if (!n) {
          if (
            Array.isArray(t) ||
            (n = (function (t, e) {
              if (!t) return;
              if ("string" == typeof t) return he(t, e);
              var n = Object.prototype.toString.call(t).slice(8, -1);
              "Object" === n && t.constructor && (n = t.constructor.name);
              if ("Map" === n || "Set" === n) return Array.from(t);
              if ("Arguments" === n || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return he(t, e);
            })(t)) ||
            (e && t && "number" == typeof t.length)
          ) {
            n && (t = n);
            var r = 0,
              i = function () {};
            return {
              s: i,
              n: function () {
                return r >= t.length ? { done: !0 } : { done: !1, value: t[r++] };
              },
              e: function (t) {
                throw t;
              },
              f: i,
            };
          }
          throw new TypeError(
            "Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."
          );
        }
        var o,
          a = !0,
          s = !1;
        return {
          s: function () {
            n = n.call(t);
          },
          n: function () {
            var t = n.next();
            return (a = t.done), t;
          },
          e: function (t) {
            (s = !0), (o = t);
          },
          f: function () {
            try {
              a || null == n.return || n.return();
            } finally {
              if (s) throw o;
            }
          },
        };
      }
      function he(t, e) {
        (null == e || e > t.length) && (e = t.length);
        for (var n = 0, r = new Array(e); n < e; n++) r[n] = t[n];
        return r;
      }
      function ve(t, e) {
        for (var n = 0; n < e.length; n++) {
          var r = e[n];
          (r.enumerable = r.enumerable || !1),
            (r.configurable = !0),
            "value" in r && (r.writable = !0),
            Object.defineProperty(t, r.key, r);
        }
      }
      function ge(t, e) {
        !(function (t, e) {
          if (e.has(t)) throw new TypeError("Cannot initialize the same private elements twice on an object");
        })(t, e),
          e.add(t);
      }
      function ye(t, e, n) {
        if (!e.has(t)) throw new TypeError("attempted to get private field on non-instance");
        return n;
      }
      pe(de, "properties", {
        widget: { type: Object },
        wazzupId: { type: String },
        integrationsService: { type: Object },
        anyActionPassed$: { type: Object },
        modalStateChange$: { type: Object },
        _animationDurationSpent: { type: Number },
      }),
        pe(
          de,
          "styles",
          $(
            te ||
              (te = re([
                "\n    .pfWidget {\n      position: fixed;\n      bottom: 24px;\n      right: 24px;\n      display: flex;\n      flex-direction: column;\n      justify-content: center;\n      align-items: center;\n      z-index: 2147483646;\n      box-sizing: border-box;\n    }\n\n    .pfWidgetInner {\n      position: relative;\n    }\n\n    .pfWidgetBtn {\n      border-radius: 50%;\n      width: calc(100% - 6px);\n      height: calc(100% - 6px);\n      display: flex;\n      align-items: center;\n      justify-content: center;\n      cursor: pointer;\n      margin: 0 0 4px;\n      z-index: 1;\n      border: none;\n      appearance: initial;\n      box-sizing: border-box;\n      padding: 0;\n      position: absolute;\n      top: 50%;\n      left: 50%;\n      transform: translate(-50%, -50%);\n      -webkit-appearance: none;\n      background-color: transparent;\n\n      @media (max-width: 960px) {\n        width: calc(100% - 3px);\n        height: calc(100% - 3px);\n      }\n    }\n\n    .pfWidgetBtnWave {\n      position: absolute;\n      top: 0;\n      left: 0;\n      right: 0;\n      bottom: 0;\n      width: 100%;\n      height: 100%;\n      border-radius: 50%;\n      animation: sonarWave 2s ease infinite;\n      pointer-events: none;\n    }\n\n    @keyframes sonarWave {\n      from {\n        opacity: 0.4;\n      }\n      to {\n        transform: scale(3);\n        opacity: 0;\n      }\n    }\n\n    .pfWidgetBtnWaveMixed {\n      animation:\n        sonarWaveMixedTelegram 2s,\n        sonarWaveMixedWhatsapp 2s,\n        sonarWaveMixedWhatsapp 2s;\n      animation-delay: 0s, 3s, 6s;\n      background: transparent;\n    }\n\n    @keyframes sonarWaveMixedTelegram {\n      from {\n        background: #0088cc;\n        opacity: 0.5;\n      }\n      to {\n        opacity: 0;\n        transform: scale(3);\n      }\n    }\n\n    @keyframes sonarWaveMixedWhatsapp {\n      from {\n        background: #25d366;\n        opacity: 0.5;\n        transform: scale(1);\n      }\n      to {\n        opacity: 0;\n        transform: scale(3);\n      }\n    }\n\n    .pfWidgetBtnWrap {\n      position: absolute;\n      z-index: 99;\n      width: 100%;\n      height: 100%;\n      transition: transform 0.5s ease;\n    }\n    \n    .pfWidgetBtnWrapMixed {\n      z-index: 102;\n    }\n    .pfWidgetBtnWrapMixed img {\n      width: calc(100% + 6px);\n      height: calc(100% + 6px);\n    }\n    .pfWidgetBtnWrapMixed .pfWidgetBtn {\n      width: 100%;\n      height: 100%;\n    }\n\n    .pfWidgetBtnWrapWhatsapp .pfWidgetBtn {\n      background: #25d366;\n    }\n\n    .pfWidgetBtnWrapTelegram .pfWidgetBtn {\n      background: #0088cc;\n    }\n    \n    .pfWidgetBtnWrapTelegram .pfWidgetBtnWave {\n      background: #0088cc;\n    }\n\n    .pfWidgetBtnWrapWhatsapp .pfWidgetBtnWave {\n      background: #25d366;\n    }\n\n    .pfWidgetBtnWrapOutline .pfWidgetBtnOutline {\n      background: #fff;\n    }\n    \n    .pfWidgetBtnWrapTelegram {\n      z-index: 100;\n    }\n\n    .pfWidgetBtnWrapWhatsapp {\n      z-index: 101;\n    }\n\n    .pfWidgetBtnWrapTelegramActive {\n      transform: translateY(-176px);\n\n      @media (max-width: 960px) {\n        transform: translateY(-96px)\n      }\n    }\n\n    .pfWidgetBtnWrapWhatsappActive {\n      transform: translateY(-88px);\n\n      @media (max-width: 960px) {\n        transform: translateY(-48px);\n      }\n    }\n\n    .pfWidgetBtnBorder {\n      position: absolute;\n      top: 0;\n      left: 0;\n      width: 100%;\n      height: 100%;\n      margin: 0;\n      border-radius: 50%;\n      box-sizing: border-box;\n    }\n\n    .pfWidgetBtnWrapTelegram .pfWidgetBtnBorder {\n      background: linear-gradient(180deg, #5ec9ff 0%, #0088cc 100%);\n    }\n\n    .pfWidgetBtnWrapWhatsapp .pfWidgetBtnBorder {\n      background: linear-gradient(180deg, #86ffb3 0%, #26d367 100%);\n    }\n\n    .pfWidgetBtnWrapOutline .pfWidgetBtnBorder {\n      background: linear-gradient(180deg, #86FFB3 0%, #26D367 100%);\n    }\n\n    .pfWidgetIcon {\n      height: 100%;\n\n      @media (max-width: 960px) {\n        height: 16px;\n        width: 16px;\n      }\n    }\n\n    /* Desktop classes */\n    .desktop_bottom-right {\n      bottom: 0;\n      right: 0;\n    }\n\n    .desktop_bottom-left {\n      bottom: 0;\n      left: 0;\n    }\n    \n    .desktop_top-right {\n      top: 0;\n      right: 0;\n    }\n    \n    .desktop_top-left {\n      top: 0;\n      left: 0;\n    }\n\n    /* Mobile classes */\n    .mobile_bottom-right {\n      bottom: 0;\n      right: 0;\n    }\n    \n    .mobile_bottom-left {\n      bottom: 0;\n      left: 0;\n    }\n    \n    .mobile_top-right {\n      top: 0;\n      right: 0;\n    }\n    \n    .mobile_top-left {\n      top: 0;\n      left: 0;\n    }\n\n    .mobile_top-left .pfWidgetBtnWrapWhatsappActive,\n    .mobile_top-right .pfWidgetBtnWrapWhatsappActive {\n      transform: translateY(88px);\n\n      @media (max-width: 960px) {\n        transform: translateY(48px);\n      }\n    }\n    .mobile_top-left .pfWidgetBtnWrapTelegramActive,\n    .mobile_top-right .pfWidgetBtnWrapTelegramActive {\n      transform: translateY(176px);\n\n      @media (max-width: 960px) {\n        transform: translateY(96px)\n      }\n    }\n  ",
              ]))
          )
        ),
        customElements.define("pf-widget", de);
      var me = new WeakSet(),
        be = new WeakSet(),
        we = (function () {
          function t(e) {
            var n, r, i;
            !(function (t, e) {
              if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
            })(this, t),
              ge(this, be),
              ge(this, me),
              (i = []),
              (r = "integrations") in (n = this)
                ? Object.defineProperty(n, r, { value: i, enumerable: !0, configurable: !0, writable: !0 })
                : (n[r] = i),
              (this.integrations = e);
          }
          var e, n, r;
          return (
            (e = t),
            (n = [
              {
                key: "insertIntegrations",
                value: function () {
                  var t,
                    e = { ym: ye(this, be, xe), ga: ye(this, me, _e) },
                    n = fe(this.integrations);
                  try {
                    for (n.s(); !(t = n.n()).done; ) {
                      var r = t.value;
                      e[r.type](r);
                    }
                  } catch (t) {
                    n.e(t);
                  } finally {
                    n.f();
                  }
                },
              },
              {
                key: "fireEvent",
                value: function (t) {
                  this.integrations.forEach(function (e) {
                    switch (e.type) {
                      case "ym":
                        window.ym(e.ym.counter, "reachGoal", t);
                        break;
                      case "ga":
                        window.gtag("event", t, { send_to: e.ga.counter });
                    }
                  });
                },
              },
              {
                key: "handleDialogEvents",
                value: function (t) {
                  t.forEach(function (t) {
                    switch (t.integrationType) {
                      case "ym":
                        window.ym(t.ym.counter, "reachGoal", "WP_dialog_whatsapp");
                        break;
                      case "ga":
                        window.gtag("event", "WP_dialog_whatsapp", { send_to: t.ga.counter });
                    }
                  });
                },
              },
            ]) && ve(e.prototype, n),
            r && ve(e, r),
            t
          );
        })();
      function _e(t) {
        var e = document.createElement("script");
        function n() {
          dataLayer.push(arguments);
        }
        (e.src = "https://www.googletagmanager.com/gtag/js?id=".concat(t.ga.counter)),
          (e.async = !0),
          document.head.appendChild(e),
          (window.dataLayer = window.dataLayer || []),
          n("js", new Date()),
          (window.gtag = n),
          window.gtag("config", t.ga.counter);
      }
      function xe(t) {
        !(function (t, e, n, r, i, o, a) {
          (t.ym =
            t.ym ||
            function () {
              (t.ym.a = t.ym.a || []).push(arguments);
            }),
            (t.ym.l = 1 * new Date()),
            (o = e.createElement(n)),
            (a = e.getElementsByTagName(n)[0]),
            (o.async = 1),
            (o.src = "https://mc.yandex.ru/metrika/tag.js"),
            a.parentNode.insertBefore(o, a);
        })(window, document, "script"),
          window.ym(t.ym.counter, "init", { webvisor: !0, clickmap: !0, accurateTrackBounce: !0, trackLinks: !0 });
      }
      var Ce = {
        every: function (t) {
          return Object.values(t).every(function (t) {
            return !0 === t;
          });
        },
        any: function (t) {
          return Object.values(t).some(function (t) {
            return !0 === t;
          });
        },
      };
      function $e(t) {
        return "function" == typeof t;
      }
      function Ae(t) {
        return function (e) {
          if (
            (function (t) {
              return $e(null == t ? void 0 : t.lift);
            })(e)
          )
            return e.lift(function (e) {
              try {
                return t(e, this);
              } catch (t) {
                this.error(t);
              }
            });
          throw new TypeError("Unable to lift unknown Observable type");
        };
      }
      var Se = function (t, e) {
        return (
          (Se =
            Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array &&
              function (t, e) {
                t.__proto__ = e;
              }) ||
            function (t, e) {
              for (var n in e) Object.prototype.hasOwnProperty.call(e, n) && (t[n] = e[n]);
            }),
          Se(t, e)
        );
      };
      function ke(t, e) {
        if ("function" != typeof e && null !== e)
          throw new TypeError("Class extends value " + String(e) + " is not a constructor or null");
        function n() {
          this.constructor = t;
        }
        Se(t, e), (t.prototype = null === e ? Object.create(e) : ((n.prototype = e.prototype), new n()));
      }
      function Ee(t, e, n, r) {
        return new (n || (n = Promise))(function (i, o) {
          function a(t) {
            try {
              c(r.next(t));
            } catch (t) {
              o(t);
            }
          }
          function s(t) {
            try {
              c(r.throw(t));
            } catch (t) {
              o(t);
            }
          }
          function c(t) {
            var e;
            t.done
              ? i(t.value)
              : ((e = t.value),
                e instanceof n
                  ? e
                  : new n(function (t) {
                      t(e);
                    })).then(a, s);
          }
          c((r = r.apply(t, e || [])).next());
        });
      }
      function We(t, e) {
        var n,
          r,
          i,
          o,
          a = {
            label: 0,
            sent: function () {
              if (1 & i[0]) throw i[1];
              return i[1];
            },
            trys: [],
            ops: [],
          };
        return (
          (o = { next: s(0), throw: s(1), return: s(2) }),
          "function" == typeof Symbol &&
            (o[Symbol.iterator] = function () {
              return this;
            }),
          o
        );
        function s(o) {
          return function (s) {
            return (function (o) {
              if (n) throw new TypeError("Generator is already executing.");
              for (; a; )
                try {
                  if (
                    ((n = 1),
                    r &&
                      (i = 2 & o[0] ? r.return : o[0] ? r.throw || ((i = r.return) && i.call(r), 0) : r.next) &&
                      !(i = i.call(r, o[1])).done)
                  )
                    return i;
                  switch (((r = 0), i && (o = [2 & o[0], i.value]), o[0])) {
                    case 0:
                    case 1:
                      i = o;
                      break;
                    case 4:
                      return a.label++, { value: o[1], done: !1 };
                    case 5:
                      a.label++, (r = o[1]), (o = [0]);
                      continue;
                    case 7:
                      (o = a.ops.pop()), a.trys.pop();
                      continue;
                    default:
                      if (!((i = a.trys), (i = i.length > 0 && i[i.length - 1]) || (6 !== o[0] && 2 !== o[0]))) {
                        a = 0;
                        continue;
                      }
                      if (3 === o[0] && (!i || (o[1] > i[0] && o[1] < i[3]))) {
                        a.label = o[1];
                        break;
                      }
                      if (6 === o[0] && a.label < i[1]) {
                        (a.label = i[1]), (i = o);
                        break;
                      }
                      if (i && a.label < i[2]) {
                        (a.label = i[2]), a.ops.push(o);
                        break;
                      }
                      i[2] && a.ops.pop(), a.trys.pop();
                      continue;
                  }
                  o = e.call(t, a);
                } catch (t) {
                  (o = [6, t]), (r = 0);
                } finally {
                  n = i = 0;
                }
              if (5 & o[0]) throw o[1];
              return { value: o[0] ? o[1] : void 0, done: !0 };
            })([o, s]);
          };
        }
      }
      Object.create;
      function Oe(t) {
        var e = "function" == typeof Symbol && Symbol.iterator,
          n = e && t[e],
          r = 0;
        if (n) return n.call(t);
        if (t && "number" == typeof t.length)
          return {
            next: function () {
              return t && r >= t.length && (t = void 0), { value: t && t[r++], done: !t };
            },
          };
        throw new TypeError(e ? "Object is not iterable." : "Symbol.iterator is not defined.");
      }
      function Me(t, e) {
        var n = "function" == typeof Symbol && t[Symbol.iterator];
        if (!n) return t;
        var r,
          i,
          o = n.call(t),
          a = [];
        try {
          for (; (void 0 === e || e-- > 0) && !(r = o.next()).done; ) a.push(r.value);
        } catch (t) {
          i = { error: t };
        } finally {
          try {
            r && !r.done && (n = o.return) && n.call(o);
          } finally {
            if (i) throw i.error;
          }
        }
        return a;
      }
      function Le(t, e, n) {
        if (n || 2 === arguments.length)
          for (var r, i = 0, o = e.length; i < o; i++)
            (!r && i in e) || (r || (r = Array.prototype.slice.call(e, 0, i)), (r[i] = e[i]));
        return t.concat(r || Array.prototype.slice.call(e));
      }
      function Te(t) {
        return this instanceof Te ? ((this.v = t), this) : new Te(t);
      }
      function Ie(t, e, n) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var r,
          i = n.apply(t, e || []),
          o = [];
        return (
          (r = {}),
          a("next"),
          a("throw"),
          a("return"),
          (r[Symbol.asyncIterator] = function () {
            return this;
          }),
          r
        );
        function a(t) {
          i[t] &&
            (r[t] = function (e) {
              return new Promise(function (n, r) {
                o.push([t, e, n, r]) > 1 || s(t, e);
              });
            });
        }
        function s(t, e) {
          try {
            !(function (t) {
              t.value instanceof Te ? Promise.resolve(t.value.v).then(c, u) : l(o[0][2], t);
            })(i[t](e));
          } catch (t) {
            l(o[0][3], t);
          }
        }
        function c(t) {
          s("next", t);
        }
        function u(t) {
          s("throw", t);
        }
        function l(t, e) {
          t(e), o.shift(), o.length && s(o[0][0], o[0][1]);
        }
      }
      function Be(t) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var e,
          n = t[Symbol.asyncIterator];
        return n
          ? n.call(t)
          : ((t = Oe(t)),
            (e = {}),
            r("next"),
            r("throw"),
            r("return"),
            (e[Symbol.asyncIterator] = function () {
              return this;
            }),
            e);
        function r(n) {
          e[n] =
            t[n] &&
            function (e) {
              return new Promise(function (r, i) {
                (function (t, e, n, r) {
                  Promise.resolve(r).then(function (e) {
                    t({ value: e, done: n });
                  }, e);
                })(r, i, (e = t[n](e)).done, e.value);
              });
            };
        }
      }
      Object.create;
      function ze(t) {
        var e = t(function (t) {
          Error.call(t), (t.stack = new Error().stack);
        });
        return (e.prototype = Object.create(Error.prototype)), (e.prototype.constructor = e), e;
      }
      var je = ze(function (t) {
        return function (e) {
          t(this),
            (this.message = e
              ? e.length +
                " errors occurred during unsubscription:\n" +
                e
                  .map(function (t, e) {
                    return e + 1 + ") " + t.toString();
                  })
                  .join("\n  ")
              : ""),
            (this.name = "UnsubscriptionError"),
            (this.errors = e);
        };
      });
      function Pe(t, e) {
        if (t) {
          var n = t.indexOf(e);
          0 <= n && t.splice(n, 1);
        }
      }
      var Ue = (function () {
          function t(t) {
            (this.initialTeardown = t), (this.closed = !1), (this._parentage = null), (this._finalizers = null);
          }
          return (
            (t.prototype.unsubscribe = function () {
              var t, e, n, r, i;
              if (!this.closed) {
                this.closed = !0;
                var o = this._parentage;
                if (o)
                  if (((this._parentage = null), Array.isArray(o)))
                    try {
                      for (var a = Oe(o), s = a.next(); !s.done; s = a.next()) {
                        s.value.remove(this);
                      }
                    } catch (e) {
                      t = { error: e };
                    } finally {
                      try {
                        s && !s.done && (e = a.return) && e.call(a);
                      } finally {
                        if (t) throw t.error;
                      }
                    }
                  else o.remove(this);
                var c = this.initialTeardown;
                if ($e(c))
                  try {
                    c();
                  } catch (t) {
                    i = t instanceof je ? t.errors : [t];
                  }
                var u = this._finalizers;
                if (u) {
                  this._finalizers = null;
                  try {
                    for (var l = Oe(u), p = l.next(); !p.done; p = l.next()) {
                      var d = p.value;
                      try {
                        Ne(d);
                      } catch (t) {
                        (i = null != i ? i : []), t instanceof je ? (i = Le(Le([], Me(i)), Me(t.errors))) : i.push(t);
                      }
                    }
                  } catch (t) {
                    n = { error: t };
                  } finally {
                    try {
                      p && !p.done && (r = l.return) && r.call(l);
                    } finally {
                      if (n) throw n.error;
                    }
                  }
                }
                if (i) throw new je(i);
              }
            }),
            (t.prototype.add = function (e) {
              var n;
              if (e && e !== this)
                if (this.closed) Ne(e);
                else {
                  if (e instanceof t) {
                    if (e.closed || e._hasParent(this)) return;
                    e._addParent(this);
                  }
                  (this._finalizers = null !== (n = this._finalizers) && void 0 !== n ? n : []).push(e);
                }
            }),
            (t.prototype._hasParent = function (t) {
              var e = this._parentage;
              return e === t || (Array.isArray(e) && e.includes(t));
            }),
            (t.prototype._addParent = function (t) {
              var e = this._parentage;
              this._parentage = Array.isArray(e) ? (e.push(t), e) : e ? [e, t] : t;
            }),
            (t.prototype._removeParent = function (t) {
              var e = this._parentage;
              e === t ? (this._parentage = null) : Array.isArray(e) && Pe(e, t);
            }),
            (t.prototype.remove = function (e) {
              var n = this._finalizers;
              n && Pe(n, e), e instanceof t && e._removeParent(this);
            }),
            (t.EMPTY = (function () {
              var e = new t();
              return (e.closed = !0), e;
            })()),
            t
          );
        })(),
        Re = Ue.EMPTY;
      function He(t) {
        return t instanceof Ue || (t && "closed" in t && $e(t.remove) && $e(t.add) && $e(t.unsubscribe));
      }
      function Ne(t) {
        $e(t) ? t() : t.unsubscribe();
      }
      var De = {
          onUnhandledError: null,
          onStoppedNotification: null,
          Promise: void 0,
          useDeprecatedSynchronousErrorHandling: !1,
          useDeprecatedNextContext: !1,
        },
        qe = {
          setTimeout: function (t, e) {
            for (var n = [], r = 2; r < arguments.length; r++) n[r - 2] = arguments[r];
            var i = qe.delegate;
            return (null == i ? void 0 : i.setTimeout)
              ? i.setTimeout.apply(i, Le([t, e], Me(n)))
              : setTimeout.apply(void 0, Le([t, e], Me(n)));
          },
          clearTimeout: function (t) {
            var e = qe.delegate;
            return ((null == e ? void 0 : e.clearTimeout) || clearTimeout)(t);
          },
          delegate: void 0,
        };
      function Fe(t) {
        qe.setTimeout(function () {
          var e = De.onUnhandledError;
          if (!e) throw t;
          e(t);
        });
      }
      function Ze() {}
      var Qe = Ye("C", void 0, void 0);
      function Ye(t, e, n) {
        return { kind: t, value: e, error: n };
      }
      var Ve = null;
      function Xe(t) {
        if (De.useDeprecatedSynchronousErrorHandling) {
          var e = !Ve;
          if ((e && (Ve = { errorThrown: !1, error: null }), t(), e)) {
            var n = Ve,
              r = n.errorThrown,
              i = n.error;
            if (((Ve = null), r)) throw i;
          }
        } else t();
      }
      var Ge = (function (t) {
          function e(e) {
            var n = t.call(this) || this;
            return (n.isStopped = !1), e ? ((n.destination = e), He(e) && e.add(n)) : (n.destination = on), n;
          }
          return (
            ke(e, t),
            (e.create = function (t, e, n) {
              return new en(t, e, n);
            }),
            (e.prototype.next = function (t) {
              this.isStopped
                ? rn(
                    (function (t) {
                      return Ye("N", t, void 0);
                    })(t),
                    this
                  )
                : this._next(t);
            }),
            (e.prototype.error = function (t) {
              this.isStopped
                ? rn(
                    (function (t) {
                      return Ye("E", void 0, t);
                    })(t),
                    this
                  )
                : ((this.isStopped = !0), this._error(t));
            }),
            (e.prototype.complete = function () {
              this.isStopped ? rn(Qe, this) : ((this.isStopped = !0), this._complete());
            }),
            (e.prototype.unsubscribe = function () {
              this.closed || ((this.isStopped = !0), t.prototype.unsubscribe.call(this), (this.destination = null));
            }),
            (e.prototype._next = function (t) {
              this.destination.next(t);
            }),
            (e.prototype._error = function (t) {
              try {
                this.destination.error(t);
              } finally {
                this.unsubscribe();
              }
            }),
            (e.prototype._complete = function () {
              try {
                this.destination.complete();
              } finally {
                this.unsubscribe();
              }
            }),
            e
          );
        })(Ue),
        Je = Function.prototype.bind;
      function Ke(t, e) {
        return Je.call(t, e);
      }
      var tn = (function () {
          function t(t) {
            this.partialObserver = t;
          }
          return (
            (t.prototype.next = function (t) {
              var e = this.partialObserver;
              if (e.next)
                try {
                  e.next(t);
                } catch (t) {
                  nn(t);
                }
            }),
            (t.prototype.error = function (t) {
              var e = this.partialObserver;
              if (e.error)
                try {
                  e.error(t);
                } catch (t) {
                  nn(t);
                }
              else nn(t);
            }),
            (t.prototype.complete = function () {
              var t = this.partialObserver;
              if (t.complete)
                try {
                  t.complete();
                } catch (t) {
                  nn(t);
                }
            }),
            t
          );
        })(),
        en = (function (t) {
          function e(e, n, r) {
            var i,
              o,
              a = t.call(this) || this;
            $e(e) || !e
              ? (i = { next: null != e ? e : void 0, error: null != n ? n : void 0, complete: null != r ? r : void 0 })
              : a && De.useDeprecatedNextContext
              ? (((o = Object.create(e)).unsubscribe = function () {
                  return a.unsubscribe();
                }),
                (i = {
                  next: e.next && Ke(e.next, o),
                  error: e.error && Ke(e.error, o),
                  complete: e.complete && Ke(e.complete, o),
                }))
              : (i = e);
            return (a.destination = new tn(i)), a;
          }
          return ke(e, t), e;
        })(Ge);
      function nn(t) {
        var e;
        De.useDeprecatedSynchronousErrorHandling
          ? ((e = t), De.useDeprecatedSynchronousErrorHandling && Ve && ((Ve.errorThrown = !0), (Ve.error = e)))
          : Fe(t);
      }
      function rn(t, e) {
        var n = De.onStoppedNotification;
        n &&
          qe.setTimeout(function () {
            return n(t, e);
          });
      }
      var on = {
        closed: !0,
        next: Ze,
        error: function (t) {
          throw t;
        },
        complete: Ze,
      };
      function an(t, e, n, r, i) {
        return new sn(t, e, n, r, i);
      }
      var sn = (function (t) {
        function e(e, n, r, i, o, a) {
          var s = t.call(this, e) || this;
          return (
            (s.onFinalize = o),
            (s.shouldUnsubscribe = a),
            (s._next = n
              ? function (t) {
                  try {
                    n(t);
                  } catch (t) {
                    e.error(t);
                  }
                }
              : t.prototype._next),
            (s._error = i
              ? function (t) {
                  try {
                    i(t);
                  } catch (t) {
                    e.error(t);
                  } finally {
                    this.unsubscribe();
                  }
                }
              : t.prototype._error),
            (s._complete = r
              ? function () {
                  try {
                    r();
                  } catch (t) {
                    e.error(t);
                  } finally {
                    this.unsubscribe();
                  }
                }
              : t.prototype._complete),
            s
          );
        }
        return (
          ke(e, t),
          (e.prototype.unsubscribe = function () {
            var e;
            if (!this.shouldUnsubscribe || this.shouldUnsubscribe()) {
              var n = this.closed;
              t.prototype.unsubscribe.call(this),
                !n && (null === (e = this.onFinalize) || void 0 === e || e.call(this));
            }
          }),
          e
        );
      })(Ge);
      function cn(t, e) {
        return Ae(function (n, r) {
          var i = 0;
          n.subscribe(
            an(r, function (n) {
              r.next(t.call(e, n, i++));
            })
          );
        });
      }
      var un = function (t) {
        return t && "number" == typeof t.length && "function" != typeof t;
      };
      function ln(t) {
        return $e(null == t ? void 0 : t.then);
      }
      var pn = ("function" == typeof Symbol && Symbol.observable) || "@@observable";
      function dn(t) {
        return t;
      }
      function fn(t) {
        return 0 === t.length
          ? dn
          : 1 === t.length
          ? t[0]
          : function (e) {
              return t.reduce(function (t, e) {
                return e(t);
              }, e);
            };
      }
      var hn = (function () {
        function t(t) {
          t && (this._subscribe = t);
        }
        return (
          (t.prototype.lift = function (e) {
            var n = new t();
            return (n.source = this), (n.operator = e), n;
          }),
          (t.prototype.subscribe = function (t, e, n) {
            var r,
              i = this,
              o =
                ((r = t) && r instanceof Ge) ||
                ((function (t) {
                  return t && $e(t.next) && $e(t.error) && $e(t.complete);
                })(r) &&
                  He(r))
                  ? t
                  : new en(t, e, n);
            return (
              Xe(function () {
                var t = i,
                  e = t.operator,
                  n = t.source;
                o.add(e ? e.call(o, n) : n ? i._subscribe(o) : i._trySubscribe(o));
              }),
              o
            );
          }),
          (t.prototype._trySubscribe = function (t) {
            try {
              return this._subscribe(t);
            } catch (e) {
              t.error(e);
            }
          }),
          (t.prototype.forEach = function (t, e) {
            var n = this;
            return new (e = vn(e))(function (e, r) {
              var i = new en({
                next: function (e) {
                  try {
                    t(e);
                  } catch (t) {
                    r(t), i.unsubscribe();
                  }
                },
                error: r,
                complete: e,
              });
              n.subscribe(i);
            });
          }),
          (t.prototype._subscribe = function (t) {
            var e;
            return null === (e = this.source) || void 0 === e ? void 0 : e.subscribe(t);
          }),
          (t.prototype[pn] = function () {
            return this;
          }),
          (t.prototype.pipe = function () {
            for (var t = [], e = 0; e < arguments.length; e++) t[e] = arguments[e];
            return fn(t)(this);
          }),
          (t.prototype.toPromise = function (t) {
            var e = this;
            return new (t = vn(t))(function (t, n) {
              var r;
              e.subscribe(
                function (t) {
                  return (r = t);
                },
                function (t) {
                  return n(t);
                },
                function () {
                  return t(r);
                }
              );
            });
          }),
          (t.create = function (e) {
            return new t(e);
          }),
          t
        );
      })();
      function vn(t) {
        var e;
        return null !== (e = null != t ? t : De.Promise) && void 0 !== e ? e : Promise;
      }
      function gn(t) {
        return $e(t[pn]);
      }
      function yn(t) {
        return Symbol.asyncIterator && $e(null == t ? void 0 : t[Symbol.asyncIterator]);
      }
      function mn(t) {
        return new TypeError(
          "You provided " +
            (null !== t && "object" == typeof t ? "an invalid object" : "'" + t + "'") +
            " where a stream was expected. You can provide an Observable, Promise, ReadableStream, Array, AsyncIterable, or Iterable."
        );
      }
      var bn = "function" == typeof Symbol && Symbol.iterator ? Symbol.iterator : "@@iterator";
      function wn(t) {
        return $e(null == t ? void 0 : t[bn]);
      }
      function _n(t) {
        return Ie(this, arguments, function () {
          var e, n, r;
          return We(this, function (i) {
            switch (i.label) {
              case 0:
                (e = t.getReader()), (i.label = 1);
              case 1:
                i.trys.push([1, , 9, 10]), (i.label = 2);
              case 2:
                return [4, Te(e.read())];
              case 3:
                return (n = i.sent()), (r = n.value), n.done ? [4, Te(void 0)] : [3, 5];
              case 4:
                return [2, i.sent()];
              case 5:
                return [4, Te(r)];
              case 6:
                return [4, i.sent()];
              case 7:
                return i.sent(), [3, 2];
              case 8:
                return [3, 10];
              case 9:
                return e.releaseLock(), [7];
              case 10:
                return [2];
            }
          });
        });
      }
      function xn(t) {
        return $e(null == t ? void 0 : t.getReader);
      }
      function Cn(t) {
        if (t instanceof hn) return t;
        if (null != t) {
          if (gn(t))
            return (
              (i = t),
              new hn(function (t) {
                var e = i[pn]();
                if ($e(e.subscribe)) return e.subscribe(t);
                throw new TypeError("Provided object does not correctly implement Symbol.observable");
              })
            );
          if (un(t))
            return (
              (r = t),
              new hn(function (t) {
                for (var e = 0; e < r.length && !t.closed; e++) t.next(r[e]);
                t.complete();
              })
            );
          if (ln(t))
            return (
              (n = t),
              new hn(function (t) {
                n.then(
                  function (e) {
                    t.closed || (t.next(e), t.complete());
                  },
                  function (e) {
                    return t.error(e);
                  }
                ).then(null, Fe);
              })
            );
          if (yn(t)) return $n(t);
          if (wn(t))
            return (
              (e = t),
              new hn(function (t) {
                var n, r;
                try {
                  for (var i = Oe(e), o = i.next(); !o.done; o = i.next()) {
                    var a = o.value;
                    if ((t.next(a), t.closed)) return;
                  }
                } catch (t) {
                  n = { error: t };
                } finally {
                  try {
                    o && !o.done && (r = i.return) && r.call(i);
                  } finally {
                    if (n) throw n.error;
                  }
                }
                t.complete();
              })
            );
          if (xn(t)) return $n(_n(t));
        }
        var e, n, r, i;
        throw mn(t);
      }
      function $n(t) {
        return new hn(function (e) {
          (function (t, e) {
            var n, r, i, o;
            return Ee(this, void 0, void 0, function () {
              var a, s;
              return We(this, function (c) {
                switch (c.label) {
                  case 0:
                    c.trys.push([0, 5, 6, 11]), (n = Be(t)), (c.label = 1);
                  case 1:
                    return [4, n.next()];
                  case 2:
                    if ((r = c.sent()).done) return [3, 4];
                    if (((a = r.value), e.next(a), e.closed)) return [2];
                    c.label = 3;
                  case 3:
                    return [3, 1];
                  case 4:
                    return [3, 11];
                  case 5:
                    return (s = c.sent()), (i = { error: s }), [3, 11];
                  case 6:
                    return c.trys.push([6, , 9, 10]), r && !r.done && (o = n.return) ? [4, o.call(n)] : [3, 8];
                  case 7:
                    c.sent(), (c.label = 8);
                  case 8:
                    return [3, 10];
                  case 9:
                    if (i) throw i.error;
                    return [7];
                  case 10:
                    return [7];
                  case 11:
                    return e.complete(), [2];
                }
              });
            });
          })(t, e).catch(function (t) {
            return e.error(t);
          });
        });
      }
      function An(t, e, n, r, i) {
        void 0 === r && (r = 0), void 0 === i && (i = !1);
        var o = e.schedule(function () {
          n(), i ? t.add(this.schedule(null, r)) : this.unsubscribe();
        }, r);
        if ((t.add(o), !i)) return o;
      }
      function Sn(t, e, n) {
        return (
          void 0 === n && (n = 1 / 0),
          $e(e)
            ? Sn(function (n, r) {
                return cn(function (t, i) {
                  return e(n, t, r, i);
                })(Cn(t(n, r)));
              }, n)
            : ("number" == typeof e && (n = e),
              Ae(function (e, r) {
                return (function (t, e, n, r, i, o, a, s) {
                  var c = [],
                    u = 0,
                    l = 0,
                    p = !1,
                    d = function () {
                      !p || c.length || u || e.complete();
                    },
                    f = function (t) {
                      return u < r ? h(t) : c.push(t);
                    },
                    h = function (t) {
                      o && e.next(t), u++;
                      var s = !1;
                      Cn(n(t, l++)).subscribe(
                        an(
                          e,
                          function (t) {
                            null == i || i(t), o ? f(t) : e.next(t);
                          },
                          function () {
                            s = !0;
                          },
                          void 0,
                          function () {
                            if (s)
                              try {
                                u--;
                                for (
                                  var t = function () {
                                    var t = c.shift();
                                    a
                                      ? An(e, a, function () {
                                          return h(t);
                                        })
                                      : h(t);
                                  };
                                  c.length && u < r;

                                )
                                  t();
                                d();
                              } catch (t) {
                                e.error(t);
                              }
                          }
                        )
                      );
                    };
                  return (
                    t.subscribe(
                      an(e, f, function () {
                        (p = !0), d();
                      })
                    ),
                    function () {
                      null == s || s();
                    }
                  );
                })(e, r, t, n);
              }))
        );
      }
      function kn(t) {
        return void 0 === t && (t = 1 / 0), Sn(dn, t);
      }
      var En = new hn(function (t) {
        return t.complete();
      });
      function Wn(t) {
        return t && $e(t.schedule);
      }
      function On(t) {
        return t[t.length - 1];
      }
      function Mn(t) {
        return Wn(On(t)) ? t.pop() : void 0;
      }
      function Ln(t, e) {
        return "number" == typeof On(t) ? t.pop() : e;
      }
      function Tn(t, e) {
        return (
          void 0 === e && (e = 0),
          Ae(function (n, r) {
            n.subscribe(
              an(
                r,
                function (n) {
                  return An(
                    r,
                    t,
                    function () {
                      return r.next(n);
                    },
                    e
                  );
                },
                function () {
                  return An(
                    r,
                    t,
                    function () {
                      return r.complete();
                    },
                    e
                  );
                },
                function (n) {
                  return An(
                    r,
                    t,
                    function () {
                      return r.error(n);
                    },
                    e
                  );
                }
              )
            );
          })
        );
      }
      function In(t, e) {
        return (
          void 0 === e && (e = 0),
          Ae(function (n, r) {
            r.add(
              t.schedule(function () {
                return n.subscribe(r);
              }, e)
            );
          })
        );
      }
      function Bn(t, e) {
        if (!t) throw new Error("Iterable cannot be null");
        return new hn(function (n) {
          An(n, e, function () {
            var r = t[Symbol.asyncIterator]();
            An(
              n,
              e,
              function () {
                r.next().then(function (t) {
                  t.done ? n.complete() : n.next(t.value);
                });
              },
              0,
              !0
            );
          });
        });
      }
      function zn(t, e) {
        if (null != t) {
          if (gn(t))
            return (function (t, e) {
              return Cn(t).pipe(In(e), Tn(e));
            })(t, e);
          if (un(t))
            return (function (t, e) {
              return new hn(function (n) {
                var r = 0;
                return e.schedule(function () {
                  r === t.length ? n.complete() : (n.next(t[r++]), n.closed || this.schedule());
                });
              });
            })(t, e);
          if (ln(t))
            return (function (t, e) {
              return Cn(t).pipe(In(e), Tn(e));
            })(t, e);
          if (yn(t)) return Bn(t, e);
          if (wn(t))
            return (function (t, e) {
              return new hn(function (n) {
                var r;
                return (
                  An(n, e, function () {
                    (r = t[bn]()),
                      An(
                        n,
                        e,
                        function () {
                          var t, e, i;
                          try {
                            (e = (t = r.next()).value), (i = t.done);
                          } catch (t) {
                            return void n.error(t);
                          }
                          i ? n.complete() : n.next(e);
                        },
                        0,
                        !0
                      );
                  }),
                  function () {
                    return $e(null == r ? void 0 : r.return) && r.return();
                  }
                );
              });
            })(t, e);
          if (xn(t))
            return (function (t, e) {
              return Bn(_n(t), e);
            })(t, e);
        }
        throw mn(t);
      }
      function jn(t, e) {
        return e ? zn(t, e) : Cn(t);
      }
      function Pn() {
        for (var t = [], e = 0; e < arguments.length; e++) t[e] = arguments[e];
        var n = Mn(t),
          r = Ln(t, 1 / 0),
          i = t;
        return i.length ? (1 === i.length ? Cn(i[0]) : kn(r)(jn(i, n))) : En;
      }
      function Un(t, e, n, r, i) {
        return function (o, a) {
          var s = n,
            c = e,
            u = 0;
          o.subscribe(
            an(
              a,
              function (e) {
                var n = u++;
                (c = s ? t(c, e, n) : ((s = !0), e)), r && a.next(c);
              },
              i &&
                function () {
                  s && a.next(c), a.complete();
                }
            )
          );
        };
      }
      function Rn(t, e) {
        return Ae(function (n, r) {
          var i = 0;
          n.subscribe(
            an(r, function (n) {
              return t.call(e, n, i++) && r.next(n);
            })
          );
        });
      }
      var Hn = Array.isArray;
      function Nn(t) {
        return cn(function (e) {
          return (function (t, e) {
            return Hn(e) ? t.apply(void 0, Le([], Me(e))) : t(e);
          })(t, e);
        });
      }
      var Dn = ["addListener", "removeListener"],
        qn = ["addEventListener", "removeEventListener"],
        Fn = ["on", "off"];
      function Zn(t, e, n, r) {
        if (($e(n) && ((r = n), (n = void 0)), r)) return Zn(t, e, n).pipe(Nn(r));
        var i = Me(
            (function (t) {
              return $e(t.addEventListener) && $e(t.removeEventListener);
            })(t)
              ? qn.map(function (r) {
                  return function (i) {
                    return t[r](e, i, n);
                  };
                })
              : (function (t) {
                  return $e(t.addListener) && $e(t.removeListener);
                })(t)
              ? Dn.map(Qn(t, e))
              : (function (t) {
                  return $e(t.on) && $e(t.off);
                })(t)
              ? Fn.map(Qn(t, e))
              : [],
            2
          ),
          o = i[0],
          a = i[1];
        if (!o && un(t))
          return Sn(function (t) {
            return Zn(t, e, n);
          })(Cn(t));
        if (!o) throw new TypeError("Invalid event target");
        return new hn(function (t) {
          var e = function () {
            for (var e = [], n = 0; n < arguments.length; n++) e[n] = arguments[n];
            return t.next(1 < e.length ? e : e[0]);
          };
          return (
            o(e),
            function () {
              return a(e);
            }
          );
        });
      }
      function Qn(t, e) {
        return function (n) {
          return function (r) {
            return t[n](e, r);
          };
        };
      }
      var Yn = (function (t) {
          function e(e, n) {
            return t.call(this) || this;
          }
          return (
            ke(e, t),
            (e.prototype.schedule = function (t, e) {
              return void 0 === e && (e = 0), this;
            }),
            e
          );
        })(Ue),
        Vn = {
          setInterval: function (t, e) {
            for (var n = [], r = 2; r < arguments.length; r++) n[r - 2] = arguments[r];
            var i = Vn.delegate;
            return (null == i ? void 0 : i.setInterval)
              ? i.setInterval.apply(i, Le([t, e], Me(n)))
              : setInterval.apply(void 0, Le([t, e], Me(n)));
          },
          clearInterval: function (t) {
            var e = Vn.delegate;
            return ((null == e ? void 0 : e.clearInterval) || clearInterval)(t);
          },
          delegate: void 0,
        },
        Xn = (function (t) {
          function e(e, n) {
            var r = t.call(this, e, n) || this;
            return (r.scheduler = e), (r.work = n), (r.pending = !1), r;
          }
          return (
            ke(e, t),
            (e.prototype.schedule = function (t, e) {
              if ((void 0 === e && (e = 0), this.closed)) return this;
              this.state = t;
              var n = this.id,
                r = this.scheduler;
              return (
                null != n && (this.id = this.recycleAsyncId(r, n, e)),
                (this.pending = !0),
                (this.delay = e),
                (this.id = this.id || this.requestAsyncId(r, this.id, e)),
                this
              );
            }),
            (e.prototype.requestAsyncId = function (t, e, n) {
              return void 0 === n && (n = 0), Vn.setInterval(t.flush.bind(t, this), n);
            }),
            (e.prototype.recycleAsyncId = function (t, e, n) {
              if ((void 0 === n && (n = 0), null != n && this.delay === n && !1 === this.pending)) return e;
              Vn.clearInterval(e);
            }),
            (e.prototype.execute = function (t, e) {
              if (this.closed) return new Error("executing a cancelled action");
              this.pending = !1;
              var n = this._execute(t, e);
              if (n) return n;
              !1 === this.pending && null != this.id && (this.id = this.recycleAsyncId(this.scheduler, this.id, null));
            }),
            (e.prototype._execute = function (t, e) {
              var n,
                r = !1;
              try {
                this.work(t);
              } catch (t) {
                (r = !0), (n = t || new Error("Scheduled action threw falsy error"));
              }
              if (r) return this.unsubscribe(), n;
            }),
            (e.prototype.unsubscribe = function () {
              if (!this.closed) {
                var e = this.id,
                  n = this.scheduler,
                  r = n.actions;
                (this.work = this.state = this.scheduler = null),
                  (this.pending = !1),
                  Pe(r, this),
                  null != e && (this.id = this.recycleAsyncId(n, e, null)),
                  (this.delay = null),
                  t.prototype.unsubscribe.call(this);
              }
            }),
            e
          );
        })(Yn),
        Gn = {
          now: function () {
            return (Gn.delegate || Date).now();
          },
          delegate: void 0,
        },
        Jn = (function () {
          function t(e, n) {
            void 0 === n && (n = t.now), (this.schedulerActionCtor = e), (this.now = n);
          }
          return (
            (t.prototype.schedule = function (t, e, n) {
              return void 0 === e && (e = 0), new this.schedulerActionCtor(this, t).schedule(n, e);
            }),
            (t.now = Gn.now),
            t
          );
        })(),
        Kn = (function (t) {
          function e(e, n) {
            void 0 === n && (n = Jn.now);
            var r = t.call(this, e, n) || this;
            return (r.actions = []), (r._active = !1), (r._scheduled = void 0), r;
          }
          return (
            ke(e, t),
            (e.prototype.flush = function (t) {
              var e = this.actions;
              if (this._active) e.push(t);
              else {
                var n;
                this._active = !0;
                do {
                  if ((n = t.execute(t.state, t.delay))) break;
                } while ((t = e.shift()));
                if (((this._active = !1), n)) {
                  for (; (t = e.shift()); ) t.unsubscribe();
                  throw n;
                }
              }
            }),
            e
          );
        })(Jn),
        tr = new Kn(Xn);
      function er(t, e, n) {
        void 0 === t && (t = 0), void 0 === n && (n = tr);
        var r = -1;
        return (
          null != e && (Wn(e) ? (n = e) : (r = e)),
          new hn(function (e) {
            var i,
              o = (i = t) instanceof Date && !isNaN(i) ? +t - n.now() : t;
            o < 0 && (o = 0);
            var a = 0;
            return n.schedule(function () {
              e.closed || (e.next(a++), 0 <= r ? this.schedule(void 0, r) : e.complete());
            }, o);
          })
        );
      }
      var nr = ze(function (t) {
        return function () {
          t(this), (this.name = "EmptyError"), (this.message = "no elements in sequence");
        };
      });
      function rr(t) {
        return t <= 0
          ? function () {
              return En;
            }
          : Ae(function (e, n) {
              var r = 0;
              e.subscribe(
                an(n, function (e) {
                  ++r <= t && (n.next(e), t <= r && n.complete());
                })
              );
            });
      }
      function ir(t) {
        return Ae(function (e, n) {
          var r = !1;
          e.subscribe(
            an(
              n,
              function (t) {
                (r = !0), n.next(t);
              },
              function () {
                r || n.next(t), n.complete();
              }
            )
          );
        });
      }
      function or(t) {
        return (
          void 0 === t && (t = ar),
          Ae(function (e, n) {
            var r = !1;
            e.subscribe(
              an(
                n,
                function (t) {
                  (r = !0), n.next(t);
                },
                function () {
                  return r ? n.complete() : n.error(t());
                }
              )
            );
          })
        );
      }
      function ar() {
        return new nr();
      }
      function sr(t) {
        return (
          (function (t) {
            if (Array.isArray(t)) return lr(t);
          })(t) ||
          (function (t) {
            if (("undefined" != typeof Symbol && null != t[Symbol.iterator]) || null != t["@@iterator"])
              return Array.from(t);
          })(t) ||
          ur(t) ||
          (function () {
            throw new TypeError(
              "Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."
            );
          })()
        );
      }
      function cr(t, e) {
        return (
          (function (t) {
            if (Array.isArray(t)) return t;
          })(t) ||
          (function (t, e) {
            var n = null == t ? null : ("undefined" != typeof Symbol && t[Symbol.iterator]) || t["@@iterator"];
            if (null == n) return;
            var r,
              i,
              o = [],
              a = !0,
              s = !1;
            try {
              for (n = n.call(t); !(a = (r = n.next()).done) && (o.push(r.value), !e || o.length !== e); a = !0);
            } catch (t) {
              (s = !0), (i = t);
            } finally {
              try {
                a || null == n.return || n.return();
              } finally {
                if (s) throw i;
              }
            }
            return o;
          })(t, e) ||
          ur(t, e) ||
          (function () {
            throw new TypeError(
              "Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."
            );
          })()
        );
      }
      function ur(t, e) {
        if (t) {
          if ("string" == typeof t) return lr(t, e);
          var n = Object.prototype.toString.call(t).slice(8, -1);
          return (
            "Object" === n && t.constructor && (n = t.constructor.name),
            "Map" === n || "Set" === n
              ? Array.from(t)
              : "Arguments" === n || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)
              ? lr(t, e)
              : void 0
          );
        }
      }
      function lr(t, e) {
        (null == e || e > t.length) && (e = t.length);
        for (var n = 0, r = new Array(e); n < e; n++) r[n] = t[n];
        return r;
      }
      var pr = {
          afterPageLoad: function (t) {
            return er(1e3 * t.seconds).pipe(
              cn(function () {
                return !0;
              })
            );
          },
          pageScrollBelow: function (t) {
            var e = t.amount,
              n = t.unit;
            return new hn(function (t) {
              t.next(r()), Zn(document, "scroll").pipe(cn(r)).subscribe(t);
            }).pipe(
              Rn(function (t) {
                return !0 === t;
              }),
              (function (t, e) {
                var n = arguments.length >= 2;
                return function (r) {
                  return r.pipe(
                    t
                      ? Rn(function (e, n) {
                          return t(e, n, r);
                        })
                      : dn,
                    rr(1),
                    n
                      ? ir(e)
                      : or(function () {
                          return new nr();
                        })
                  );
                };
              })()
            );
            function r() {
              var t = document.body,
                r = document.documentElement,
                i = Math.max(t.scrollHeight, t.offsetHeight, r.clientHeight, r.scrollHeight, r.offsetHeight),
                o = window.scrollY;
              return "px" === n ? o >= e : "pct" === n ? (o / (i - window.innerHeight)) * 100 >= e : void 0;
            }
          },
          location: function (t) {
            var e = t.matcher,
              n = t.values,
              r = {
                includes: function (t, e) {
                  return t.every(function (t) {
                    return e.includes(t);
                  });
                },
                not_includes: function (t, e) {
                  return t.every(function (t) {
                    return !e.includes(t);
                  });
                },
                equals: function (t, e) {
                  return t.every(function (t) {
                    return t === e;
                  });
                },
                not_equals: function (t, e) {
                  return t.every(function (t) {
                    return t !== e;
                  });
                },
                starts_with: function (t, e) {
                  return t.every(function (t) {
                    return e.startsWith(t);
                  });
                },
              };
            function i() {
              var t = window.location.href.replace(/\/+$/, "");
              return (0, r[e])(n, t);
            }
            return new hn(function (t) {
              t.next(i()), Zn(window, "locationchange").pipe(cn(i)).subscribe(t);
            });
          },
          device: function (t) {
            return (function () {
              for (var t = [], e = 0; e < arguments.length; e++) t[e] = arguments[e];
              var n = Mn(t);
              return jn(t, n);
            })(t.deviceType === (i() ? "mobile" : "desktop"));
          },
        },
        dr = function (t) {
          var e = cr(Object.entries(t.query), 1),
            n = cr(e[0], 2),
            r = n[0],
            i = n[1],
            o = i.map(function (t) {
              return (0, pr[t.type])(t[t.type]).pipe(
                cn(function (e) {
                  return { type: t.type, isPassed: e };
                })
              );
            }),
            a = Object.fromEntries(
              i.map(function (t) {
                return [t.type, !1];
              })
            );
          return Pn.apply(void 0, sr(o)).pipe(
            (function (t, e) {
              return Ae(Un(t, e, arguments.length >= 2, !0));
            })(function (t, e) {
              return (t[e.type] = e.isPassed), t;
            }, a),
            Rn(function (t) {
              return (function (t, e) {
                return Ce[t](e);
              })(r, t);
            }),
            cn(function () {
              return t;
            })
          );
        };
      function fr(t) {
        return (
          (function (t) {
            if (Array.isArray(t)) return hr(t);
          })(t) ||
          (function (t) {
            if (("undefined" != typeof Symbol && null != t[Symbol.iterator]) || null != t["@@iterator"])
              return Array.from(t);
          })(t) ||
          (function (t, e) {
            if (!t) return;
            if ("string" == typeof t) return hr(t, e);
            var n = Object.prototype.toString.call(t).slice(8, -1);
            "Object" === n && t.constructor && (n = t.constructor.name);
            if ("Map" === n || "Set" === n) return Array.from(t);
            if ("Arguments" === n || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return hr(t, e);
          })(t) ||
          (function () {
            throw new TypeError(
              "Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."
            );
          })()
        );
      }
      function hr(t, e) {
        (null == e || e > t.length) && (e = t.length);
        for (var n = 0, r = new Array(e); n < e; n++) r[n] = t[n];
        return r;
      }
      var vr,
        gr = ze(function (t) {
          return function () {
            t(this), (this.name = "ObjectUnsubscribedError"), (this.message = "object unsubscribed");
          };
        }),
        yr = (function (t) {
          function e() {
            var e = t.call(this) || this;
            return (
              (e.closed = !1),
              (e.currentObservers = null),
              (e.observers = []),
              (e.isStopped = !1),
              (e.hasError = !1),
              (e.thrownError = null),
              e
            );
          }
          return (
            ke(e, t),
            (e.prototype.lift = function (t) {
              var e = new mr(this, this);
              return (e.operator = t), e;
            }),
            (e.prototype._throwIfClosed = function () {
              if (this.closed) throw new gr();
            }),
            (e.prototype.next = function (t) {
              var e = this;
              Xe(function () {
                var n, r;
                if ((e._throwIfClosed(), !e.isStopped)) {
                  e.currentObservers || (e.currentObservers = Array.from(e.observers));
                  try {
                    for (var i = Oe(e.currentObservers), o = i.next(); !o.done; o = i.next()) {
                      o.value.next(t);
                    }
                  } catch (t) {
                    n = { error: t };
                  } finally {
                    try {
                      o && !o.done && (r = i.return) && r.call(i);
                    } finally {
                      if (n) throw n.error;
                    }
                  }
                }
              });
            }),
            (e.prototype.error = function (t) {
              var e = this;
              Xe(function () {
                if ((e._throwIfClosed(), !e.isStopped)) {
                  (e.hasError = e.isStopped = !0), (e.thrownError = t);
                  for (var n = e.observers; n.length; ) n.shift().error(t);
                }
              });
            }),
            (e.prototype.complete = function () {
              var t = this;
              Xe(function () {
                if ((t._throwIfClosed(), !t.isStopped)) {
                  t.isStopped = !0;
                  for (var e = t.observers; e.length; ) e.shift().complete();
                }
              });
            }),
            (e.prototype.unsubscribe = function () {
              (this.isStopped = this.closed = !0), (this.observers = this.currentObservers = null);
            }),
            Object.defineProperty(e.prototype, "observed", {
              get: function () {
                var t;
                return (null === (t = this.observers) || void 0 === t ? void 0 : t.length) > 0;
              },
              enumerable: !1,
              configurable: !0,
            }),
            (e.prototype._trySubscribe = function (e) {
              return this._throwIfClosed(), t.prototype._trySubscribe.call(this, e);
            }),
            (e.prototype._subscribe = function (t) {
              return this._throwIfClosed(), this._checkFinalizedStatuses(t), this._innerSubscribe(t);
            }),
            (e.prototype._innerSubscribe = function (t) {
              var e = this,
                n = this,
                r = n.hasError,
                i = n.isStopped,
                o = n.observers;
              return r || i
                ? Re
                : ((this.currentObservers = null),
                  o.push(t),
                  new Ue(function () {
                    (e.currentObservers = null), Pe(o, t);
                  }));
            }),
            (e.prototype._checkFinalizedStatuses = function (t) {
              var e = this,
                n = e.hasError,
                r = e.thrownError,
                i = e.isStopped;
              n ? t.error(r) : i && t.complete();
            }),
            (e.prototype.asObservable = function () {
              var t = new hn();
              return (t.source = this), t;
            }),
            (e.create = function (t, e) {
              return new mr(t, e);
            }),
            e
          );
        })(hn),
        mr = (function (t) {
          function e(e, n) {
            var r = t.call(this) || this;
            return (r.destination = e), (r.source = n), r;
          }
          return (
            ke(e, t),
            (e.prototype.next = function (t) {
              var e, n;
              null === (n = null === (e = this.destination) || void 0 === e ? void 0 : e.next) ||
                void 0 === n ||
                n.call(e, t);
            }),
            (e.prototype.error = function (t) {
              var e, n;
              null === (n = null === (e = this.destination) || void 0 === e ? void 0 : e.error) ||
                void 0 === n ||
                n.call(e, t);
            }),
            (e.prototype.complete = function () {
              var t, e;
              null === (e = null === (t = this.destination) || void 0 === t ? void 0 : t.complete) ||
                void 0 === e ||
                e.call(t);
            }),
            (e.prototype._subscribe = function (t) {
              var e, n;
              return null !== (n = null === (e = this.source) || void 0 === e ? void 0 : e.subscribe(t)) && void 0 !== n
                ? n
                : Re;
            }),
            e
          );
        })(yr),
        br = (function (t) {
          function e(e) {
            var n = t.call(this) || this;
            return (n._value = e), n;
          }
          return (
            ke(e, t),
            Object.defineProperty(e.prototype, "value", {
              get: function () {
                return this.getValue();
              },
              enumerable: !1,
              configurable: !0,
            }),
            (e.prototype._subscribe = function (e) {
              var n = t.prototype._subscribe.call(this, e);
              return !n.closed && e.next(this._value), n;
            }),
            (e.prototype.getValue = function () {
              var t = this,
                e = t.hasError,
                n = t.thrownError,
                r = t._value;
              if (e) throw n;
              return this._throwIfClosed(), r;
            }),
            (e.prototype.next = function (e) {
              t.prototype.next.call(this, (this._value = e));
            }),
            e
          );
        })(yr);
      function wr(t, e) {
        return (
          (function (t) {
            if (Array.isArray(t)) return t;
          })(t) ||
          (function (t, e) {
            var n = null == t ? null : ("undefined" != typeof Symbol && t[Symbol.iterator]) || t["@@iterator"];
            if (null == n) return;
            var r,
              i,
              o = [],
              a = !0,
              s = !1;
            try {
              for (n = n.call(t); !(a = (r = n.next()).done) && (o.push(r.value), !e || o.length !== e); a = !0);
            } catch (t) {
              (s = !0), (i = t);
            } finally {
              try {
                a || null == n.return || n.return();
              } finally {
                if (s) throw i;
              }
            }
            return o;
          })(t, e) ||
          (function (t, e) {
            if (!t) return;
            if ("string" == typeof t) return _r(t, e);
            var n = Object.prototype.toString.call(t).slice(8, -1);
            "Object" === n && t.constructor && (n = t.constructor.name);
            if ("Map" === n || "Set" === n) return Array.from(t);
            if ("Arguments" === n || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _r(t, e);
          })(t, e) ||
          (function () {
            throw new TypeError(
              "Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."
            );
          })()
        );
      }
      function _r(t, e) {
        (null == e || e > t.length) && (e = t.length);
        for (var n = 0, r = new Array(e); n < e; n++) r[n] = t[n];
        return r;
      }
      var xr = "__buttonly_id",
        Cr = 730;
      !(function () {
        !(function () {
          if (!0 !== window[s]) {
            window[s] = !0;
            var t = history.pushState;
            history.pushState = function () {
              var e = t.apply(this, arguments);
              return window.dispatchEvent(new Event("pushstate")), window.dispatchEvent(new Event("locationchange")), e;
            };
            var e = history.replaceState;
            history.replaceState = function () {
              var t = e.apply(this, arguments);
              return (
                window.dispatchEvent(new Event("replacestate")), window.dispatchEvent(new Event("locationchange")), t
              );
            };
          }
        })();
        var t = document.currentScript.dataset.pfId;
        Promise.all([u(t), p(t), d(t), f(t)]).then(function (e) {
          var n = wr(e, 4),
            r = n[0],
            i = n[1],
            o = n[2],
            a = n[3];
          if (r) {
            var s;
            ((s = document.createElement("link")).rel = "stylesheet"),
              (s.href = "https://fonts.googleapis.com/css2?family=Ubuntu:wght@400;500;700&display=swap"),
              document.head.appendChild(s);
            var u,
              p,
              f =
                ((p = new br(!1)),
                (window.pfWidget = {
                  open: function () {
                    return p.next(!0);
                  },
                  close: function () {
                    return p.next(!1);
                  },
                }),
                {
                  modalStateChange$: p.pipe(
                    ((u = 1),
                    Rn(function (t, e) {
                      return u <= e;
                    }))
                  ),
                }),
              h = (function () {
                !(function () {
                  var t = m.get(xr);
                  t && t.includes("-") && m.set(xr, t.replace("-", ""), { expires: Cr });
                })();
                var t = v("123456789", 8)();
                m.get(xr) || m.set(xr, t, { expires: Cr });
                return m.get(xr);
              })(),
              g = new we(i);
            g.insertIntegrations(),
              g.handleDialogEvents(o),
              setInterval(function () {
                d(t).then(function (t) {
                  return g.handleDialogEvents(t);
                });
              }, 9e4);
            var y = (function (t) {
              var e = t.map(function (t) {
                return dr(t);
              });
              return Pn.apply(void 0, fr(e));
            })(a);
            !(function (t) {
              var e = t.widget,
                n = t.wazzupId,
                r = t.integrationsService,
                i = t.anyActionPassed$,
                o = t.modalStateChange$,
                a = K(
                  vr ||
                    ((s = [
                      '\n    <pf-widget\n   id="widget"   .widget="',
                      '"\n      .wazzupId="',
                      '"\n      .integrationsService="',
                      '"\n      .anyActionPassed$="',
                      '"\n      .modalStateChange$="',
                      '"\n    ></pf-widget>\n  ',
                    ]),
                    c || (c = s.slice(0)),
                    (vr = Object.freeze(Object.defineProperties(s, { raw: { value: Object.freeze(c) } })))),
                  e,
                  n,
                  r,
                  i,
                  o
                );
              var s, c;
              rt(a, document.body);
            })({
              widget: r,
              wazzupId: h,
              integrationsService: g,
              anyActionPassed$: y,
              modalStateChange$: f.modalStateChange$,
            }),
              l(t, { name: "enter", wazzupId: h });
          } else
            !(function () {
              for (var t, e = arguments.length, n = new Array(e), r = 0; r < e; r++) n[r] = arguments[r];
              (t = console).error.apply(t, [c].concat(n));
            })("Can not found widget with id", t);
        });
      })();
    })();
})();
